classdef Parameters_OL < Parameters
	properties (SetAccess = protected, GetAccess = public)		
		NB_TIRAGES         % config
		VALUES_STORING;    % config
		
		% Dimensions and values
		NB_DIM_S; 		
		NB_INIT_O;         % config
		NB_OBJ           
						
		% Initialisation P_S_knowing_O
		INIT_MU; 
		INIT_SIGMA;        % c
		INIT_POIDS;        % c
		
		% Maj P_S_knowing_O
		CHOICE_O;         % c
		CHOICE_LING;
		POIDS;            % c
		
		MASTER;
	end
	
	methods (Access = protected)		
		function init_variables(obj,file_name)
			obj.NB_TIRAGES = Parsing.parsing_value(file_name,'NB_TIRAGES'); 
			
			obj.INIT_SIGMA = Parsing.parsing_value(file_name,'INIT_SIGMA');
			obj.INIT_POIDS = Parsing.parsing_value(file_name,'INIT_POIDS');
			obj.NB_INIT_O = Parsing.parsing_value(file_name,'NB_INIT_O');
			
			obj.CHOICE_O = Parsing.parsing_value(file_name,'CHOICE_O');
			obj.POIDS = Parsing.parsing_value(file_name,'POIDS');
			obj.VALUES_STORING = Parsing.parsing_value(file_name,'VALUES_STORING');
		end

		function init_dimension(obj,nb_dim,minimum,maximum)
			obj.NB_DIM_S = nb_dim;
			
			MIN_S = minimum;
			MAX_S = maximum;
			
			obj.INIT_MU = zeros(1,obj.NB_DIM_S);
			for i=1:obj.NB_DIM_S
				obj.INIT_MU(i) = rand()*(MAX_S(i) - MIN_S(i)) + MIN_S(i);
			end	
		end
	end
			
	methods
		function obj = Parameters_OL(file_name_global,file_name_special,file_master)
			obj@Parameters(file_name_global);
			obj.init_variables(file_name_special);
			obj.MASTER = Master(file_master);
		end
		
		function initialise_dimensions(obj,nb_dim,minimum,maximum)
			obj.init_dimension(nb_dim,minimum,maximum);
		end
		
		function set_choice_ling (obj,r)
			obj.CHOICE_LING = r;
		end
		
		function set_nb_init_o (obj,r)
			obj.NB_INIT_O = r;
		end
	end

end