classdef Parameters < handle
	properties (SetAccess = protected, GetAccess = public)
		% Global
		SIMU_MIN; 
		SIMU_MAX; 
		SEED;
		
		REP;
		REP_PARAM;
	end
	
	methods (Access = protected)
		function init_parameters(obj,file_name)			
			obj.SIMU_MIN = Parsing.parsing_value(file_name,'SIMU_MIN');
			obj.SIMU_MAX = Parsing.parsing_value(file_name,'SIMU_MAX');
			obj.REP = Parsing.parsing_txt(file_name,'REP');
			obj.REP_PARAM = Parsing.parsing_txt(file_name,'REP_PARAM');					
		end
	end
	
	methods (Abstract, Access = protected)
		init_variables(obj,file_name);		
	end
			
	methods
		function obj = Parameters(file_name_global)
			obj.init_parameters(file_name_global);	
		end
		
		function set_seed (obj,s)
			obj.SEED = s;
		end
		
		function set_rep (obj,r)
			obj.REP = r;
		end
		
		function set_rep_param (obj,r)
			obj.REP_PARAM = r;
		end

	end

end