% Script test form and GMM
clear all;
close all;
%clc;

addpath('./functions');
addpath('./classes');
for Ng=[49]
    for V=[1]
    tirages = 600000;
        GSyl1 = 7;
        GSyl2 = 5;
        stp = 20;
        simu_id = sprintf('Seq3_Sl_Fast_wordsyl_%d-%d_v%d_%d',GSyl1,GSyl2,V,Ng);
        rep = ['./data/' simu_id '/'];
        disp_rep = ['./display/' simu_id '/'];        

    %% Initialisation

        for sd=4:5
            %% Parameters structurescript_wo
            % To put else where take too much space ..... :p
            wrd_pmt = struct( ...
                'name',             'Word',...
                'type',             'Loi de succession',...
                'nb_elem',          GSyl1*GSyl2, ...
                'nb_components',    Ng, ...
                'step',             stp,...
                'tirages',          tirages,...
                'sampling',         [0:0.1:10], ...
                'values_storing',   [0 1 5 10 15 20 40 50 75 100 150 200 250 300 500 1000 1500 2000 2500 3000 3500 4000 5000 7000 8000 9000 10000 15000 20000 25000 30000 35000 40000 45000 50000 60000 70000 100000 125000 150000 175000 200000 250000 300000 400000:200000:4000000], ...
                'data_input',       [rep 'input/' num2str(sd) '/'], ...
                'data_output',      [rep 'output/' num2str(sd) '/'], ...
                'disp_output',      [disp_rep num2str(sd) '/'],...
                'legend_x',         'Mots'...,      
            );

            gmm_pmt = struct( ...
                'name',         'Gsyl',...
                'type',         'Mixture de gaussiennes',...
                'nb',           Ng, ...
                'dim',          2, ...   
                'mu',           [7 13;2.5 6]', ...
                'cov',          [0.5 0;0 0.5], ...
                'n_init',       25, ...
                'method',       'update_max', ...
                'init_method',  'regular', ...
                'learning',     V,...
                'ch_iter',      150000,...
                'tirages',          tirages,...
                'sampling',         [0:0.1:10], ...
                'values_storing',   [0 1 5 10 15 20 40 50 75 100 150 200 250 300 500 1000 1500 2000 2500 3000 3500 4000 5000 7000 8000 9000 10000 15000 20000 25000 30000 35000 40000 45000 50000 60000 70000 100000 125000 150000 175000 200000 250000 300000 400000:200000:4000000], ...
                'data_input',       [rep 'input/' num2str(sd) ], ...
                'data_output',      [rep 'output/' num2str(sd)], ...
                'disp_output',      [disp_rep num2str(sd) '/'],...
                'measure',          4 + 2^2 + 2,...
                'legend_x',         'F1',...
                'legend_y',         'F2'...,
            );
            rng(sd);

            %%   Input data (Master data)

            %% Init forms
            disp('Charging parameters...')
            gmm1 = Mixture_Gaussienne(); 
            gmm1.initialize('G1',gmm_pmt);
            gmm1.init_directories();
            gmm1.load_input(GSyl1,GSyl2);

            wrd = Succession_Rule();
            wrd.initialize('W',wrd_pmt);
            wrd.init_directories();
            wrd.load_input(GSyl1,GSyl2);
            disp('End of init');

            % Learning / Fitting
            if tirages > length(gmm1.data_input)
                error('Insufficient data!');
            end

            gmm1.launch_iteration(0,2);
            wrd.launch_iteration(0,[],[]);
            for s_id=1:gmm_pmt.ch_iter
                g1 = gmm1.launch_iteration(s_id,1);
                g2 = gmm1.launch_iteration(s_id,2);
                wrd.launch_iteration(-1,g1,g2);
            end
            
            gmm1.switch_learning();
            
            for s_id=gmm_pmt.ch_iter+1:gmm_pmt.tirages
                w = wrd.get_word_index(s_id);
                %this thing need to go out ...
                psg1 = gmm1.get_proba(gmm1.data_input(s_id,:,1)').*(gmm1.weight_gaussians.');
                psg2 = gmm1.get_proba(gmm1.data_input(s_id,:,2)').*(gmm1.weight_gaussians.');
                Pgx = wrd.get_proba(w).*(psg1*psg2');
                [~,ind] = max(Pgx(:));
                [g1 g2] = ind2sub(size(Pgx),ind);

                g1 = gmm1.launch_iteration(s_id,1,g1);
                g2 = gmm1.launch_iteration(s_id,2,g2);
                wrd.launch_iteration(s_id,g1,g2);
            end
            
            gmm1.save_simulation_into_file();
            wrd.save_simulation_into_file();

           s1 = Display_form();
           w = Display_form();
            
           s1.initialize(gmm_pmt,'G1',3,GSyl1,GSyl2);
           w.initialize(wrd_pmt,'W',0,GSyl1,GSyl2);
            
           s1.display_distribution(gmm_pmt);
           w.display_distribution(wrd_pmt);
        
      	end
    end
end