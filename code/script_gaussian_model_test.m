clear all;
close all;
%clc;

addpath('./functions');
addpath('./classes');

simu_id = 8;
rep = sprintf('./data/gaussian_model_test/%d/',simu_id);
disp_rep = sprintf('./display/gaussian_model_test/%d/',simu_id);

%% Parameters structure

%TODO add to parameters class
global PMT ;
PMT = struct( ...
             'nb_tirages',     100 , ...
             'input',          [1,1.3; 9,2], ...			% mu and sigma (standard deviation)
             'chosen_method',  'update_all', ...
             'nb',             2, ...
             'dim',            1, ...
             'var',            7, ...
             'mu',             'random', ...
             'max_m',          12, ...
             'min_m',          0, ...
             'data_weight',    20, ...
             'rep',            rep, ...
             'disp_rep',       disp_rep, ...
             'seed',           5, ...
             'values_storing', [0 1 2 3 4 5 10 20 50 100 5000] ...  %20000 50000 1000000]);
            );


%% Initialisation
disp('Charging parameters...')
rng(PMT.seed);
gmm = Gaussian_model_test();

gmm.init_directories();
disp('End of init');

%% Computations

gmm.launch_simulation(PMT);
                    
disp('Begin display...');
gmm.display_simulation_results();

%end
