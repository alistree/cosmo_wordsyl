clear all;
clc;

%--- Chemin ---%
%addpath('../../master');
addpath('./functions');
addpath('./classes');
addpath('./parameters');

global PMT;

%--- Repertoires ---%
rep_param = './';	
rep_data_r = './data/';
rep_display_r = './display/';
rep_parameters = './parameters/data/';

disp('Chargement des parametres');
param_global = sprintf('%sParameters.txt',rep_parameters);
param_special = sprintf('%sParameters_OL.txt',rep_parameters);
param_master = sprintf('%sParameters_master_files.txt',rep_parameters);
PMT = Parameters_OL(param_global,param_special,param_master);

for i=PMT.SIMU_MIN:PMT.SIMU_MAX
	
	PMT.set_seed(i);
	rng(PMT.SEED);
	
	%tab_init_o = [3,4,5,6,7,8,9,10,12,15,20,21,22,25,30];
    % A modifier
	tab_init_o = [2];
	
	for j=1:length(tab_init_o)
		
		nb_o = tab_init_o(j);
		PMT.set_nb_init_o(nb_o);

		rep_data = sprintf('%ssimu_%d/%d/P_S_knowing_OL/',rep_data_r,i,nb_o);
		rep_display = sprintf('%ssimu_%d/%d/P_S_knowing_OL/normalise_',rep_display_r,i,nb_o);
		
		Display_PS_knowing_OL.display_PS_knowing_OL_O(rep_param,rep_data,rep_display,length(PMT.VALUES_STORING));
		%Display_PS_knowing_OL.display_PS_knowing_OL_F(rep_param,rep_data,rep_display,length(PMT.VALUES_STORING));
		%Display_PS_knowing_OL.display_PS_knowing_OL_S(rep_param,rep_data,rep_display,length(PMT.VALUES_STORING));
	
	end
end
