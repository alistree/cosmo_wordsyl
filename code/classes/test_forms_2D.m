% Script test form and GMM
clear all;
close all;
%clc;
addpath('./../functions');

% simu_id = 10;
% rep = sprintf('./data/gaussian_model_test/%d/',simu_id);
% disp_rep = sprintf('./display/gaussian_model_test/%d/',simu_id);

%% Parameters structure

                    

%% Initialisation
%'mu',        [7 13;2.5 6]', ...
%for var = [0.1 0.25 0.5 1 2 4 5 7.5 10 12.5 15] 
for var = [ 0.25] 

    for sd=1:1
        id =sprintf('g50-max_N25_C0-5_%d',sd);
        gmm_pmt = struct( ...
                'nb',        50, ...
                'dim',       2, ...   
                'mu',        [7 13;2.5 6]', ...
                'cov',       [0.5 0;0 0.5], ...
                'n_init',    25, ...
                'method',    'update_max' ... ,
                );

        forms_pmt = struct( ...
                    'tirages',          10000,...
                    'sampling',         [0:0.1:10], ...
                    'values_storing',   [0 2 5 15 20 40 50 75 100 150 200 250 300 500 1000 1500 2000 2500 3000 3500 4000 5000 7000 8000 9000 10000 15000 20000 25000 30000 35000 50000 60000 70000 100000], ...
                    'data_input',       ['./test_forms/input/' id '/'], ...
                    'data_output',      ['./test_forms/output/' id '/'], ...
                    'disp_output',      ['./test_forms/display/' id '/']...
                    );
        disp_pmt = struct( ...
                    'name',             'test_2D',...
                    'type',             'GM',...
                    'tirages',          forms_pmt.tirages,...
                    'space_dim',        gmm_pmt.dim,...
                    'elm',              gmm_pmt.nb,...
                    'measure',          4 + gmm_pmt.dim^2 + gmm_pmt.dim,...
                    'iter',             forms_pmt.values_storing,...
                    'data_input',       forms_pmt.data_input,...
                    'data_output',      forms_pmt.data_output,...
                    'disp_output',      forms_pmt.disp_output,...
                    'sampling',         forms_pmt.sampling,...
                    'legend_x',         'F1',...
                    'legend_y',         'F2'...,
                );

        rng(sd);

        %%   Origibnal_data
        % input_mu = [2 2;9 9];
        % input_cov(:,:,1) = [1 0; 0 1];
        % input_cov(:,:,2) = [1 0; 0 1];
        % data_vect = [];
        % ratio = [0.5 0.5];
        % for i= 1:size(input_cov,3)
        %   data_vect =  [ data_vect; mvnrnd( input_mu(i,:), ...
        %                                     input_cov(:,:,i), ...
        %                                     ceil(forms_pmt.tirages*ratio(i)))];
        % end
        % ordering = randperm(length(data_vect));
        % data_vect = data_vect(ordering,:);
        %figure;hist(data_vect,100);

        data_vect = [];
        for i=1:7
            data_vect = [data_vect; load(sprintf('../vowel/data_PB_S_knowing_OS_%d.txt',i))];
        end
        data_vect = 7*asinh(data_vect/650);
        
        %hyp_shwa = mvnrnd( [4.5 9.5], [0.02 0;0 0.02], 5000);
        %data_vect = [data_vect; hyp_shwa];

        ordering1 = randperm(length(data_vect));
        ordering2 = randperm(length(data_vect));
        tmp_mat = data_vect(ordering1,:);
        tmp_mat2 = data_vect(ordering2,:);
        tmp_mat = [tmp_mat; tmp_mat2];
        data_vect = tmp_mat;
        %tmp = hyp_shwa(1:20,:)
        %data_vect = [tmp; data_vect];
        %figure;plot(data_vect(:,1),data_vect(:,2),'.');

        IOControl.create_repertory(forms_pmt.data_input);
        delete([forms_pmt.data_input 'data_vect.txt']);
        IOControl.save([forms_pmt.data_input 'data_vect.txt'],data_vect);

        %% script
        disp('Charging parameters...')
        gmm = Mixture_Gaussienne();
        gmm.initialize('test',forms_pmt,gmm_pmt);
        gmm.init_directories();
        gmm.load_input();

        for i=0:forms_pmt.tirages
            gmm.launch_iteration(i);
        end
        c = gmm.save_simulation_into_file();
        s = Display_form();
        s.initialize(disp_pmt);
        s.display_distribution(disp_pmt);

        disp('End of init');
    end
end
