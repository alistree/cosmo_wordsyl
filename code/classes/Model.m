classdef Model
    
    properties (Static, GetAccess=public, SetAccess=protected)
        % Variables
        % Decomposition
        forms;
        nb_forms;
        parameters = struct{};
        input_data = './';        
    end
    
    methods(Static, Access = public)
        % Constructor
        function Model(varargin)
            if mod(nargin-1)
                error('Bad arguments at model initialization!');
            end
            parameters = varargin{1};
            obj.nb_forms = (nargin-1)/2;
            obj.forms = Form.empty(obj.nb_forms,0);
            for i=1:obj.nb_forms
                obj.forms(i)=Form(varargin{2*i},varargin{2*i+1}); ...
                % type puis nom de la forme parametrique
            end            
        end
        
        function init_forms()
            for i=1:obj.nb_forms
                obj.forms(i).initialize();
            end
        end
        
        function initialize(varargin)
            obj.input_data = varargin{1};
            obj.init_forms();
        end
        
        
        function update_form(index)
            obj.forms(index).update();
        end
        
        function save_form(index)
            state = obj.forms(index).to_string();
        end
