classdef Master < handle
	properties (SetAccess = protected, GetAccess = public)
		% Gestionnaire des syllabes
		m_syl;
		
		% Master distrib
		master_vowel;
		master_consonant;		
	end
	
	methods (Access = protected)	
	
		function initialise_master_vowel(obj,file_param_master)
			rep_master_vow = Parsing.parsing_txt(file_param_master,'REP_MASTER_VOW');
			file_master_vow = Parsing.parsing_txt(file_param_master,'FILE_MASTER_VOW');
			
			%Charger les valeurs du maître
			for i=1: obj.m_syl.get_nb_vow()
				mat_load = load(sprintf('%s%s_%d',rep_master_vow,file_master_vow,i));
				field_name = fieldnames(mat_load);
				matrix(:,:) = getfield(mat_load,field_name{1});	
				obj.master_vowel(i,:,:) = matrix;
			end
		end
		
		function initialise_master_consonant(obj,file_param_master)
			rep_master_cons = Parsing.parsing_txt(file_param_master,'REP_MASTER_CONS');
			file_master_cons = Parsing.parsing_txt(file_param_master,'FILE_MASTER_CONS');	
			
			%Charger les valeurs du maître
			num = 1;
			for i=1: obj.m_syl.get_nb_vow()
				for j=1: obj.m_syl.get_nb_cons()
					vow = obj.m_syl.get_vow(i);
					cons = obj.m_syl.get_cons(j);
					syl = sprintf('%s%s',cons,vow);
					
					file_cons = sprintf(file_master_cons,syl);
					mat_load = load(sprintf('%s%s/%s',rep_master_cons,vow,file_cons));
					field_name = fieldnames(mat_load);
					matrix = getfield(mat_load,field_name{1});
					
					obj.master_consonant{obj.m_syl.get_indice_syllabe(i,j)} = matrix;
				end
			end
		end
	end
	
	methods 
		function obj = Master(file_param_master)
			obj.m_syl = Manage_Syllabe();
			obj.initialise_master_vowel(file_param_master);
			%obj.initialise_master_consonant(file_param_master);
		end
		
		function ind_obj = get_ind_obj_suivant(obj,ind_obj,choice_ling)
			nb_obj = 0;
			if strcmp(choice_ling,obj.m_syl.TYPE_SYL)
				nb_obj = obj.m_syl.get_nb_syl();		
			elseif strcmp(choice_ling,obj.m_syl.TYPE_CONS)
				% On met ici le nombre de syllabe puisque master_consonant est basé sur le nombre de syllabe
				% Ca ne change rien, on aura tout de même les 3 syllabes /b/, /d/ et /g/ mais elles seront proportionnellement attachée aux 7 voyelles.
				nb_obj = obj.m_syl.get_nb_syl();
			elseif strcmp(choice_ling,obj.m_syl.TYPE_VOW)
				nb_obj = obj.m_syl.get_nb_vow();
			end
			
			ind_obj = ind_obj + 1;
			if ind_obj > nb_obj
				ind_obj = 1;
			end
		end
	
		% Pas utilisé actuellement mais peut servir %
		function set_master_distrib(obj,mast_vow,mast_cons)
			clear obj.master_vowel;
			clear obj.master_consonant;
			
			obj.master_vowel = mast_vow;
			obj.master_consonant = mast_cons;
		end
		
		function s = get_master(obj,ind_obj,choice_ling)
		% PMT.master_vowel : matrice 3D avec dim 1 : voyelle, dim 2 : indice, dim 3 : sons
		% PMT.master_consonant : structure de matrice dont l'indice donne la syllabe
		% Chaque matrice est 2D avec dim 1 : indice, dim 2 : sons
		
			if strcmp(choice_ling,obj.m_syl.TYPE_SYL)	
				[ind_vow,ind_cons] = obj.m_syl.get_indice_vow_cons(ind_obj);
				mat_cons = obj.master_consonant{ind_obj};
				indice = randi(size(mat_cons,1)); % Tirage d'une des consonnes
				lim = size(mat_cons,2)-1; 
				sf_tmp = mat_cons(indice,2:lim); % Récup de la consonne (tableau 2D)
				sf = reshape(sf_tmp,1,size(sf_tmp,2)); % Pour faire un tableau 1D
				
				ind_point = mat_cons(indice,end); % Récup de la ligne du tableau de la voyelle
				so_tmp = obj.master_vowel(ind_vow,ind_point,:); % Récup de la voyelle (tableau 2D)
				so = reshape(so_tmp,1,size(so_tmp,3)); %Pour faire un tableau 1D
				s = [sf,so];
				s = 7*asinh(s/650); % En Barks
				
			elseif strcmp(choice_ling,obj.m_syl.TYPE_CONS)
				mat_cons = obj.master_consonant{ind_obj};
				indice = randi(size(mat_cons,1)); % Tirage d'une consonne
				lim = size(mat_cons,2)-1;
				s_tmp = mat_cons(indice,2:lim);
				s = reshape(s_tmp,1,size(s_tmp,2));
				s = 7*asinh(s/650); % En Barks
				
			elseif strcmp(choice_ling,obj.m_syl.TYPE_VOW)	
				indice = randi(size(obj.master_vowel,2)); % Tirage d'une voyelle
				s_tmp = obj.master_vowel(ind_obj,indice,:);
				s = reshape(s_tmp,1,size(s_tmp,3));
				s = 7*asinh(s/650); % En Barks
			end
		end
		
		function [master_vow_distrib,master_cons_distrib] = get_distrib_master_obj(obj)
			rep_param = '../../';
			[master_vowel_entropy,master_cons_entropy,unique_ind_vow,unique_ind_cons] = obj.get_entropy_master();
			
			for i=1:obj.m_syl.get_nb_vow()
				[minimum,step,maximum,nb_values] = Dimension.give_dimensions_S_vowel_discretized(rep_param);
				mast_vow(:,:) = obj.master_vowel(i,:,:);
				mast_vow_form(:,1) = 7*asinh(mast_vow(:,1)/650);
				mast_vow_form(:,2) = 7*asinh(mast_vow(:,2)/650);
				chosen_case_vow =  Discretization.search_case_tt(mast_vow_form,step,minimum);
				chosen_ind_vow = Discretization.give_indice_tt(chosen_case_vow,size(obj.master_vowel,2));		
				%chosen_ind_vow_tt = [chosen_ind_vow_tt;chosen_ind_vow];		
				master_vow_distrib(:,i) = hist(chosen_ind_vow,unique_ind_vow)./length(chosen_ind_vow);
			end
					
			for j=1:obj.m_syl.get_nb_syl()
				[minimum,step,maximum,nb_values] = Dimension.give_dimensions_S_cons_discretized(rep_param);
				mast_cons_tmp = obj.master_consonant{j};
				mast_cons = mast_cons_tmp(:,[2:3]);
				mast_cons_form = [];
				mast_cons_form(:,1) = 7*asinh(mast_cons(:,1)./650);
				mast_cons_form(:,2) = 7*asinh(mast_cons(:,2)./650);
				chosen_case_cons =  Discretization.search_case_tt(mast_cons_form,step,minimum);
				chosen_ind_cons = Discretization.give_indice_tt(chosen_case_cons,size(obj.master_consonant,2));
				%chosen_ind_cons_tt = [chosen_ind_cons_tt;chosen_ind_cons];	
				master_cons_distrib(:,j) = hist(chosen_ind_cons,unique_ind_cons)./length(chosen_ind_cons);		
			end	
			
		end
		
		function [master_vowel_entropy,master_cons_entropy,unique_ind_vow,unique_ind_cons] = get_entropy_master(obj)
			rep_param = '../../';
			
			chosen_ind_vow_tt = [];
			for i=1:obj.m_syl.get_nb_vow()
				[minimum,step,maximum,nb_values] = Dimension.give_dimensions_S_vowel_discretized(rep_param);
				mast_vow(:,:) = obj.master_vowel(i,:,:);
				mast_vow_form(:,1) = 7*asinh(mast_vow(:,1)/650);
				mast_vow_form(:,2) = 7*asinh(mast_vow(:,2)/650);
				chosen_case_vow =  Discretization.search_case_tt(mast_vow_form,step,minimum);
				chosen_ind_vow = Discretization.give_indice_tt(chosen_case_vow,size(obj.master_vowel,2));		
				chosen_ind_vow_tt = [chosen_ind_vow_tt;chosen_ind_vow];	
			end
			unique_ind_vow = unique(chosen_ind_vow_tt);	
			hist_master_vow = hist(chosen_ind_vow_tt,unique_ind_vow)./length(chosen_ind_vow_tt);
			master_vowel_entropy = - sum(hist_master_vow .* log2(hist_master_vow));
			
			chosen_ind_cons_tt = [];
			for j=1:obj.m_syl.get_nb_syl()
				[minimum,step,maximum,nb_values] = Dimension.give_dimensions_S_cons_discretized(rep_param);
				mast_cons_tmp = obj.master_consonant{j};
				mast_cons = mast_cons_tmp(:,[2:3]);
				mast_cons_form = [];
				mast_cons_form(:,1) = 7*asinh(mast_cons(:,1)./650);
				mast_cons_form(:,2) = 7*asinh(mast_cons(:,2)./650);
				chosen_case_cons =  Discretization.search_case_tt(mast_cons_form,step,minimum);
				chosen_ind_cons = Discretization.give_indice_tt(chosen_case_cons,size(obj.master_consonant,2));
				chosen_ind_cons_tt = [chosen_ind_cons_tt;chosen_ind_cons];	
			end	
			unique_ind_cons = unique(chosen_ind_cons_tt);
			hist_master_cons = hist(chosen_ind_cons_tt,unique_ind_cons)./length(chosen_ind_cons_tt);
			master_cons_entropy = - sum(hist_master_cons .* log2(hist_master_cons));
		end
	end
end