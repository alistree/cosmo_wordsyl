classdef Display_form < Display_vowel
    %DISPLAY_FORM Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        name;
        type;
        space_dim;
        sampling;
        tirages;
        iterations;
        disp_output;
        pmt;
        input_data;
        hist;
        hist_index;
        output_data;
        output_path;
    end
    
    methods
        function obj = Display_form()
        end
        
        function initialize(obj,pmt,nn,type,GSyl1,GSyl2)
            obj.name = nn; %pmt.name;
            obj.type = pmt.type;
            obj.tirages = pmt.tirages;
            obj.disp_output = [pmt.disp_output '/' obj.name '/'];
            obj.output_path = [pmt.data_output '/' obj.name '/'];
            obj.iterations = pmt.values_storing;
            dt_id = sprintf('%d-%d_',GSyl1,GSyl2);
            switch obj.type
              case 'Mixture de gaussiennes'
                obj.space_dim = pmt.dim;
                obj.sampling = pmt.sampling;
                tmp = load([obj.output_path 'gm_output_data.txt']);
                obj.output_data = reshape(tmp, pmt.nb, pmt.measure, []);            
                raw_input = load(['vowel/' dt_id 'syl_master_data_rand.txt']); % Big brother is watching you :)
                if type==1
                    obj.input_data = raw_input(:,type:type+1);
                elseif type==2
                    obj.input_data = raw_input(:,type+1:type+2);
                elseif type==3
                    obj.input_data = zeros(2*size(raw_input,1),2);
                    obj.input_data(:,1) = reshape(raw_input(1:end,[1,3])',[],1);
                    obj.input_data(:,2) = reshape(raw_input(1:end,[2,4])',[],1); 
                else
                    raw_input = reshape(raw_input,[],2);
                    obj.input_data = raw_input;
                end
                s = pmt.sampling; size(s);
                obj.hist_index = s(1:10:end);
                obj.hist = hist(obj.input_data(:,:,1),obj.hist_index);
                obj.hist = obj.hist/sum(obj.hist);
              case 'Loi de succession'
                tmp = load([obj.output_path 'sr_output_data.txt']);
                obj.output_data = reshape(tmp,pmt.nb_components,pmt.nb_components,pmt.nb_elem,[]);
                obj.input_data = load(['vowel/' dt_id 'wrd_master_data_rand.txt']);
                obj.hist_index = 1:pmt.nb_elem;
                obj.hist = hist(obj.input_data,obj.hist_index);
                obj.hist = obj.hist/sum(obj.hist);
            end
        end
        
        function [values,w,mu,cov] = compute_form(obj)
            if strcmp(obj.type,'Mixture de gaussiennes')
                 dim = obj.output_data(1,3,1);
                  x = obj.output_data(1,1,:); x = reshape(x,1,size(x,3));
                  w = obj.output_data(:,2,:); w = reshape(w,size(w,1),1,size(w,3));
                  mu = obj.output_data(:,4:(3+dim),:);
                  cov = obj.output_data(:,4+dim:3+dim+dim^2,:);
                  cov = permute(reshape(permute(cov,[2 1 3]),...
                                        dim,dim,[],size(cov,3)),...
                                [2 1 3 4]);
                  values = zeros(length(obj.sampling),length(obj.sampling),size(x,1));
                  size(values)
                  for j=1:size(x,2)
                      if obj.space_dim == 1
                          X = (obj.sampling).';
                          values(:,:,j) =  pdf(gmdistribution(mu(:,:,j),cov(:,:,:,j),w(:,:,j)),X);
                      elseif obj.space_dim == 2
                          [X Y] = meshgrid(obj.sampling,obj.sampling+5*ones(size(obj.sampling)));
                          values(:,:,j) = reshape(pdf(...
                              gmdistribution(mu(:,:,j), cov(:,:,:,j),...
                                             w(:,:,j)),[X(:) Y(:)]),...
                                                  size(X));
                      end
                   end
                
            else
                values = 0;
            end
        end
        
        function h = compute_entropy(obj,values)
        % Entropy computation H = Sum (P log(P))
            ds = (obj.sampling(2)-obj.sampling(1))^obj.space_dim;
            
            % Replacing zero-values with epislon+Normalisation
            values(values==0)=eps; 
            values = values./repmat(sum(sum(values)),[size(values,1) size(values,2) 1]);
            mixt = values.*log(values);   %values is a 2D map ofprobabilities, the
                                             %third dimension correspond to iterations
            
            h = reshape(-sum(sum(mixt(:,:,:))),1,[],1);
            save([obj.output_path 'agent_entropy.mat'],'h');
        end
        
        function [div h_master] = compute_kl_divergence(obj,values)
        % Symmetric divergence computation D = 0.5 (sum(P1 log(P1/P2))+
            edges{1} = obj.sampling; edges{2} = obj.sampling+5*ones(size(obj.sampling));
            % Agent distribution
            values = values./repmat(sum(sum(values)),[size(values,1),size(values,2),1]);
            % Master distribution from input data histogram
            %h = size(obj.input_data,1)*0.5;
            %ord = reshape([1:h;h+1:2*h],1,[]);
            %input = obj.input_data(ord,:);
            p_master = hist3(obj.input_data(1:obj.tirages,:),'Edges', edges); %%TODO Attentiion!!!!
            
            % Replacing zero-values with epislon+Normalisation
            p_master(p_master==0) = eps;
            p_master = p_master'/sum(sum(p_master));
            % Master entropy calculated without zero-values
            h_master = -sum(sum(p_master.*log(p_master)));
            
            % tmp is input data histogram replicated for each iteration
            % to make divergence plot easier ...
            tmp = repmat(p_master,[1,1,size(values,3)]);
            
            % Replacing zero-values with epislon+Normalisation
            values(values==0) = eps;
            values = values./repmat(sum(sum(values)),[size(tmp,1),size(tmp,2),1]);
            
            %Symmetric expression of KLdivergence
            div1 = values.*(log(values)- log(tmp));
            div2 = tmp.*(log(tmp)- log(values));
            div = 0.5*(div1 + div2);
            
            % well shaped row-vector , value at each iteration (column)
            div = reshape(sum(sum(div(:,:,:))),1,[],1);
            div1 = reshape(sum(sum(div1(:,:,:))),1,[],1);
            div2 = reshape(sum(sum(div2(:,:,:))),1,[],1);
            %figure; 
            %plot(obj.iterations(1:size(div,2)),[div;div1;div2]);
            %legend('Symmetric KL Divergence','D_{KL}(Mixture||Input)','D_{KL}(Input||Mixture)');
            %title('Kullback-Liebler divergence');
            %xlabel('Iterations');
            h_master = h_master*ones(size(div));
            save([obj.output_path 'kl_div.mat'],'div');
            save([obj.output_path 'master_entropy.mat'],'h_master');
        end
        
        function display_distribution_iteration(obj,title,values,pmt,iter,means)
            if obj.space_dim == 1
                x = (pmt.sampling).';
                obj.display_plot_1D_plushist(values',x,title,pmt,obj.hist,obj.hist_index);
            elseif obj.space_dim == 2
                [x y] = meshgrid(pmt.sampling,pmt.sampling+5*ones(size(pmt.sampling)));
                obj.display_surface_2D(values,x,y,title,pmt);
                obj.display_contour_2D(values,obj.input_data(1:iter,:),x,y,[title],pmt,means);
            else
                error('Space_dim error (not defined)');
            end    
        end
        
        function display_distribution(obj,pmt)
            if strcmp(obj.type,'Mixture de gaussiennes')
                  [values weights means covar] = obj.compute_form();
                  h_mixt = obj.compute_entropy(values);
                  [kl_div h_master]  = obj.compute_kl_divergence(values);
                  obj.display_dynamic_measure(weights,means,covar,h_mixt,h_master,kl_div);
  
                  for i=[1,size(values,3)]   % display last state
                      obj.display_distribution_iteration([obj.disp_output ...
                                          sprintf('fig_%d',obj.iterations(i))],...
                                                         values(:,:,i),pmt,...
                                                         obj.iterations(i),means(:,:,i));
                      
                      obj.display_bar(weights(:,:,i),sprintf([obj.disp_output ...
                                         sprintf('bar_%d',obj.iterations(i)) ]));
             end
            elseif strcmp(obj.type,'Loi de succession')
                n= pmt.nb_components;
                map = obj.output_data;
                [x y]=meshgrid(1:n);
                %map(:,:,:,end)
%                   for i=1:size(map,3)
%                       v = abs(log(map(:,:,i,end)));
%                       
%                       f1 = figure;
%                       set(f1, 'Visible', 'off');
%                       b=bar3(v);view([0 90]);
%                       colorbar
%                       colormap(hot)
%                       for k = 1:length(b)
%                           zdata = get(b(k),'ZData');
%                           set(b(k),'CData',zdata);
%                           set(b(k),'FaceColor','interp');
%                       end
%                       xlabel('$GSyl_2$','Interpreter','latex');
%                       ylabel('$GSyl_1$','Interpreter','latex');
%                       title(sprintf('$|ln(P(GSyl_1~GSyl_2~|~[W=w_{%d}]))|$',i),'Interpreter','latex');
%                       print(f1,'-dpng',[obj.disp_output sprintf('end_word_%d',i)]);    
%                 end
                map = obj.output_data;
                d = sum(map,3);
                h = -sum(sum(d.*log(d)));
                save([obj.output_path 'word_entropy.mat'],'h');
                  
                f3 = figure;
                  set(f3, 'Visible', 'off');
                  plot(obj.iterations(1:size(h,4)),reshape(h,1,[]));
                  xlabel('Iterations');
                  ylabel('entropy');
                  title('Entropy Evolution of $\sum_W P(GSyl_1~GSyl_2~|~W)$','Interpreter','latex');
                  print(f3,'-dpng',[obj.disp_output 'entropy_word']);
                
                
                d = d(:,:,:,46);   % 46 for 600000
                d = d/sum(sum(d));
                all_bars = length(d(d>0.00001));
                major_bars = length(d(d>0.01));
                mean_yellow = sum(sum(d(d<0.01)));
                if isnan(mean_yellow)
                    mean_yellow = 0;
                end
                
                x = cumsum(d(:,:,:)>0.01);
                y = cumsum(d(:,:,:)'>0.01)';
                col = x(end,:); lin = y(:,end);
                cl = [length(lin(lin>0)),length(col(col>0))];
                
                inc = col.*lin';
                inc = length(inc(inc>0));
                
                save([obj.output_path 'line_col.mat'],'cl');
                save([obj.output_path 'all_bars.mat'],'all_bars');
                save([obj.output_path 'major_bars.mat'],'major_bars');
                save([obj.output_path 'mean_yellow.mat'],'mean_yellow');
                save([obj.output_path 'inc.mat'],'inc');
                
                d = abs(log(d));
               
%                 for i=1:size(d,4)
%                     d(:,:,:,i) = d(:,:,:,i)/sum(sum(d(:,:,:,i)));
%                     tmp = d(:,:,:,i);
%                     all_bars(i) = length(tmp(tmp>0.00001));
%                     major_bars(i) = length(tmp(tmp>0.01));
%                 end
% 
%                 save([obj.output_path 'all_bars.mat'],'all_bars');
%                 save([obj.output_path 'major_bars.mat'],'major_bars');
               
%                   f2 = figure;
%                   set(f2, 'Visible', 'off');
%                   d=bar3(d);view([0 90]);
%                   colormap(hot)
%                   for k = 1:length(d)
%                       zdata = get(d(k),'ZData');
%                       set(d(k),'CData',zdata);
%                       set(d(k),'FaceColor','interp');
%                   end
%                   xlabel('$G_2$','Interpreter','latex');
%                   ylabel('$G_1$','Interpreter','latex');
%                   c = colorbar;
%                   %set(c, 'YScale', 'log');
%                   title('$\displaystyle\sum_{W}~P(G_1~G_2~|~W)$','Interpreter','latex');
%                   print(f2,'-dpng',[obj.disp_output 'Sum_end_word']);
                  
%                   fb = figure;
%                   set(fb, 'Visible', 'off');
%                   b = bar([all_bars major_bars]);
%                   set(gca,'xticklabel',{'>.0001','>.01'});
%                   xlabel('\sum_{W}~P(G_1~G_2~|~W)','interpreter','latex');
%                   print(fb,'-dpng',[obj.disp_output 'number_kern']);
            end
        end
        
        function display_dynamic_measure(obj,weights,means,covar,h_mixt,h_master,div)
            w = permute(weights, [3 1 2]);
            mu = permute(means, [3 1 2]); 
            h = zeros(size(covar,3),size(covar,4));
            for i=1:size(covar,4)
                for j=1:size(covar,3)
                    h(j,i) = 0.5*log((2*pi*exp(1))^2*det(covar(:,:,j,i)));
                end
            end

            obj.display_multiplot(w(:,:,1),'P(G)',...
                                  mu(:,:,1),'Means',...
                                  h','Individual Entropy',...
                                  obj.iterations(1:size(w,1)),...
                                  [obj.disp_output 'dynamic']);

            obj.display_multiplot2(div, 'Symmetric KL-Divergence',...
                                  [h_mixt;h_master], 'Global entropy & Master entropy',...
                                  obj.iterations(1:size(w,1)),...
                                  [obj.disp_output 'measures']);
        end
	end
end