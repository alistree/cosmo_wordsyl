classdef Form < handle
    properties(GetAccess = public, SetAccess = protected)
        name;
        type;
        space_dim = 0;
        
        data_input_path;
        data_output_path;
        disp_output_path;
        
        data_input;
        data_output;
        
        iter;
        values_storing;
    end

    
    methods
        
        function obj = Form()
        end
        
        function initialize(obj,name, type, pmt)
%             if nargin ~= 3
%                 error(['Inappropriate argments, Please give name, ' ...
%                        'type and parameters structure']);
%             end
            obj.name = name;
            obj.type = type;
            obj.data_input_path = pmt.data_input;
            obj.data_output_path = [ pmt.data_output '/' obj.name '/'];
            obj.disp_output_path = [ pmt.disp_output '/' obj.name '/'];
            obj.values_storing = pmt.values_storing;
            obj.iter = 0;
            
            %parameters = varargin{3} %( nb iter
                                         % data_input
                                         % data_output
                                         % disp_output
                                         % for gmm 
                                         % nb_gauss, space dim,
                                         % mean(max_min), variance, n_init,
                                         % chosen method
                                         % sampling, seed,,values storing,)
        end
        
        function set_space_dim(obj,n)
% $$$             if ~isinteger(d)
% $$$                 error('Dimension value is not correct');
% $$$             end
            obj.space_dim = n;
        end
        
        function dim = get_space_dim(obj)
            dim = obj.space_dim;
        end
        
        function init_directories(obj)
            data_dir = [obj.data_output_path  '/'];
            disp_dir = [obj.disp_output_path  '/'];
            IOControl.create_repertory(data_dir);
            IOControl.create_repertory(disp_dir);
            
            if size(ls(disp_dir)) ~= [0 0]
                delete([disp_dir '*']);
            end
            if size(ls(data_dir)) ~= [0 0]    
                delete([data_dir '*']);
            end
        end    
        
        function load_input(obj,GSyl1,GSyl2)
            dt_id = sprintf('%d-%d_',GSyl1,GSyl2);

           if strcmp(obj.type,'Mixture de gaussiennes')
                data = load(['vowel/' dt_id 'syl_master_data_rand.txt']); %% TOREPAIR
                obj.data_input = reshape(data,size(data,1),2,2);
           elseif strcmp(obj.type,'Loi de succession')
                data = load(['vowel/' dt_id 'wrd_master_data_rand.txt']);
                obj.data_input = reshape(data,size(data,1),3);
           end
        end
    
        function save_simulation_iteration(obj,iter)
            sz = size(obj.data_output,2);
            obj.data_output{sz+1} = obj.to_string(iter);
        end

        function c=save_simulation_into_file(obj)
            data_dir = [obj.data_output_path];
            if strcmp(obj.type,'Mixture de gaussiennes')
                file_name = 'gm_output_data.txt';
                if exist([data_dir file_name],'file')
                    delete([data_dir file_name '.txt']);
                end
                c = reshape(cell2mat(obj.data_output),...
                                    size(obj.data_output{1},1),...
                                    size(obj.data_output{1},2),...
                                    size(obj.data_output,2));
                IOControl.save([data_dir file_name], c);
            elseif strcmp(obj.type ,'Loi de succession')
                file_name = 'sr_output_data.txt';
                if exist([data_dir file_name],'file')
                    delete([data_dir file_name '.txt']);
                end
                size(obj.data_output)
                c = cell2mat(obj.data_output);
                IOControl.save([data_dir file_name], c);
            end
            

        end

        function output = launch_iteration(obj,varargin)
            output=0;
            if obj.iter==0
                    fprintf('Saving data %d...\n',0);
                    obj.save_simulation_iteration(0);
            else
                if strcmp(obj.type, 'Mixture de gaussiennes')
                    id = varargin{1}; syl = varargin{2}; 
                    if obj.learning == 2
                        pgw = varargin{3};
                        output = obj.add_point(obj.data_input(id,:,syl).',pgw);
                    elseif obj.learning == 3
                         g = varargin{3};
                        output = obj.add_point(obj.data_input(id,:,syl).',g);
                    else
                        output = obj.add_point(obj.data_input(id,:,syl).');
                    end
                    if ismember(obj.iter, obj.values_storing)
                        fprintf('GM_Saving data %d...\n',obj.iter);
                        obj.save_simulation_iteration(obj.iter);
                    end
                elseif strcmp(obj.type, 'Loi de succession')
                    id = varargin{1};
                    g1 = varargin{2}; g2 = varargin{3};
                    if (id~= -1)
                        obj.add_point(obj.data_input(id,1),g1,g2);
                    end
                    if ismember(obj.iter, obj.values_storing)
                        fprintf('SR_Saving data %d...\n',obj.iter);
                        obj.save_simulation_iteration(obj.iter);
                    end
                end
            end
            obj.iter = obj.iter + 1;
        end
        
        
    end
end
        
% $$$     methods
% $$$     function get_proba(obj)
% $$$     end
% $$$     function get_entropy(obj)
% $$$     end
% $$$     function save(obj)
% $$$     end
