classdef IOControl
	methods	(Static)
		%--- Sauvegarde de fichiers ---%
		
		function save(name,var)
			if exist(name,'file')
				dlmwrite(name,var,'-append');
			else
				dlmwrite(name,var);
			end
		end
        
		function save_text(name,var)
			fileID = fopen(name,'a+');
			fprintf(fileID,var);
			fclose(fileID);
		end
		
		function create_repertory(name)
			if ~exist(name)
				mkdir(name);
			end
		end
		
        function write(varargin)
            dlmwrite(varargin{1},varargin{2},varargin{3},varargin{4},varargin{5});
        end
	end
	
end