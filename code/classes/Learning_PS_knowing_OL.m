classdef Learning_PS_knowing_OL
	methods (Static, Access = protected)
		
		% ------------------------------------ %
		% ---------- Initialisation ---------- %
		% ------------------------------------ %
		
		%--- Initialisation of P_S_knowing_O 
		function [P_O_weight,P_S_knowing_O] = init_P_S_knowing_O();
			global PMT;
			
			P_S_knowing_O = Mixture_Gaussienne(PMT.NB_INIT_O,PMT.NB_DIM_S,PMT.INIT_MU,PMT.INIT_SIGMA,PMT.INIT_POIDS);			 
			P_O_weight = ones(PMT.NB_INIT_O,1) ./ PMT.NB_INIT_O; % Initialisés Poids unitaires
		end
		
		function random_data = init_random_data(choice_o,nb_tirages)
			global PMT;
			
			if choice_o == 1 || choice_o == 4
				random_data = zeros(nb_tirages,PMT.NB_DIM_S+1);
			elseif choice_o == 2 || choice_o == 3
				random_data = zeros(nb_tirages,PMT.NB_DIM_S+2*PMT.NB_INIT_O);
			end		
		end
		
		% ------------------------------------ %
		% ------------ Selection ------------- %
		% ------------------------------------ %
		
		%--- Selection of the sound 		
		function [selected_s,ind_obj] = select_new_S(ind_obj,choice_ling)
			global PMT;
			
			selected_s = PMT.MASTER.get_master(ind_obj,choice_ling);			
			ind_obj = PMT.MASTER.get_ind_obj_suivant(ind_obj,choice_ling);
		end
	
	
		%--- Selection of the object "o"
		function P_O = select_new_O(P_S_knowing_O,P_O_weight,s,choice)
			if choice == 1                                                  % In case of choosing the maximum of the distribution
				[P_O_knowing_S, ind_max_P_O_knowing_S] = Learning_PS_knowing_OL.select_max_O(P_S_knowing_O,P_O_weight,s);
				P_O = ind_max_P_O_knowing_S;	
			elseif choice == 2                                              % Choosing the whole distribution
				P_O = Learning_PS_knowing_OL.select_distrib_P_OL_knowing_S(P_S_knowing_O,P_O_weight,s);
			elseif choice == 3                                              % Choosing the whole distribution
				[P_O_knowing_S, ind_max_P_O_knowing_S] = Learning_PS_knowing_OL.select_max_O(P_S_knowing_O,P_O_weight,s);
				P_O.distrib = P_O_knowing_S;
				P_O.max = ind_max_P_O_knowing_S;
			elseif choice == 4
				P_O_knowing_S = Learning_PS_knowing_OL.select_distrib_P_OL_knowing_S(P_S_knowing_O,P_O_weight,s);
				distrib_P_O_knowing_S = Distribution('m',P_O_knowing_S);
				P_O = distrib_P_O_knowing_S.draw_i();
			else
				P_O = [];
				error('Error in the choice of method to select a new object');
			end
		end
		
		function P_O_knowing_S = select_distrib_P_OL_knowing_S(P_S_knowing_O,P_O_weight,s)						
			P_O_knowing_S_t = P_S_knowing_O.get_proba(s) .* P_O_weight;
			sum_P_O_knowing_S = sum(P_O_knowing_S_t);
			P_O_knowing_S = P_O_knowing_S_t ./ sum_P_O_knowing_S;		
		end	
		
		function [P_O_knowing_S, ind_max_P_O_knowing_S] = select_max_O(P_S_knowing_O,P_O_weight,s)		
			P_O_knowing_S = Learning_PS_knowing_OL.select_distrib_P_OL_knowing_S(P_S_knowing_O,P_O_weight,s);
			[val_max_P_O_knowing_S , ind_max_P_O_knowing_S] = max(P_O_knowing_S);
		end
		
		% ------------------------------------ %
		% ------------- Saving --------------- %
		% ------------------------------------ %
		
		function save_learning_P_S_knowing_O(random_data,P_S_knowing_O,P_O_weight,nb_o)
			global PMT;
			
			% Save all_data
			dlmwrite(sprintf('%srandom_data_moteur.txt',PMT.REP),random_data);
					
			% Enregistrement des paramètres de P(S|OL)
			parameters = P_S_knowing_O.get_parameters();
			for k=1:nb_o
				IOControl.save(sprintf('%sparameters_P_S_knowing_OL_%d.txt',PMT.REP,k),parameters(k,:));
			end
			IOControl.save(sprintf('%shist_P_O_weight_%d.txt',PMT.REP,k),P_O_weight');
		end

	end
	
	methods (Static)
	
		function P_S_knowing_O = generate_incremental_P_S_knowing_O()			
			global PMT;
			
            % Chargement des parametres------------
			nb_tirages = PMT.NB_TIRAGES;
			choice_o = PMT.CHOICE_O;
			nb_o = PMT.NB_INIT_O;
			choice_ling = PMT.CHOICE_LING;
			
            % Initialisation de la mixture de gaussiennes
			[P_O_weight,P_S_knowing_O] = Learning_PS_knowing_OL.init_P_S_knowing_O();
			random_data = Learning_PS_knowing_OL.init_random_data(choice_o,nb_tirages);
			
            
			j=1;
			ind_obj = 1;
			for i = 1:nb_tirages
                % fprintf('tirage %d \r',i);
				% Pour chaque nouvelle donnee ( S et O)
				[selected_s,ind_obj] = Learning_PS_knowing_OL.select_new_S(ind_obj,choice_ling);				
								
				P_O = Learning_PS_knowing_OL.select_new_O(P_S_knowing_O,P_O_weight,selected_s,choice_o);
				
				rdt = [selected_s];
				if choice_o == 1 || choice_o == 4
					P_S_knowing_O.add_point(P_O,selected_s,PMT.POIDS);
                    %P_S_knowing_O.add_point(P_O,selected_s,PMT.POIDS*P_O_weight(P_O));
               
                    % Partage de la contribution de la donn�e entrante.
                    %for selected_o = 1:nb_o
                        %if selected_o ~= P_O
                     %       P_S_knowing_O.add_point(selected_o,selected_s,PMT.POIDS/nb_o);
                        %end
                    % Ouuuuuuuulala
                    
                    %end     
					random_data(i,:) = [rdt,P_O];
				elseif choice_o == 2
					for selected_o = 1:nb_o
						P_S_knowing_O.add_point(selected_o,selected_s,P_O(selected_o));
						rdt = [rdt,selected_o,P_O(selected_o)];
					end
					random_data(i,:) = rdt;
				elseif choice_o == 3					
					for selected_o = 1:nb_o
						if selected_o == P_O.max
							P_S_knowing_O.add_point(selected_o,selected_s,PMT.POIDS + P_O.distrib(selected_o));
						else
							P_S_knowing_O.add_point(selected_o,selected_s,P_O.distrib(selected_o));
						end
						rdt = [rdt,selected_o,P_O.distrib(selected_o)];
					end
					random_data(i,:) = rdt;
				end
				
                % La distribution P(g) -histogramme mais fausse actuellement
				P_O_weight(P_O) = P_O_weight(P_O) + 0.00001;
				P_O_weight = P_O_weight ./ sum(P_O_weight);
				
				if j<= length(PMT.VALUES_STORING) && PMT.VALUES_STORING(j) == i				
					j = j + 1;
					
					Learning_PS_knowing_OL.save_learning_P_S_knowing_O(random_data,P_S_knowing_O,P_O_weight,nb_o);			
				end
			end
			
		end
	
	end
	
end