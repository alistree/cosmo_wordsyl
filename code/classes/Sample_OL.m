classdef Sample_OL

	methods(Static, Access = protected)	
		function param = get_param(repertory,num)
			file_name = sprintf('%sparameters/param_display_P_S_knowing_OL_%d.txt',repertory,num);			
			param.nb_dim_s = Parsing.parsing_value(file_name,'NB_DIM_S')
			param.nb_init_o = Parsing.parsing_value(file_name,'NB_INIT_O')
		end
		
		function tab_ind = perform_redim_2D_1D(tab_ind_ligne,tab_ind_colonne)	
			if ~ isempty(tab_ind_ligne)
				[X,Y] = meshgrid(tab_ind_colonne,tab_ind_ligne);
				X = reshape(X,prod(size(X)),1);
				Y = reshape(Y,prod(size(Y)),1);
				tab_ind = [Y,X];
			end
		end
	end
	
	methods(Static)
	
		function [param,rep_data,rep_display] = initialize_param(m_syl,type,rep_data,rep_display)
			ind_choice_ling = m_syl.get_ind_type_obj(type);
			choice_ling = type;
			
			rep_data_temp = sprintf('%s%s/',rep_data,choice_ling);
			param = Sample_OL.get_param(rep_data_temp,ind_choice_ling);
			
			rep_data = sprintf('%s%s/',rep_data,choice_ling);
			rep_display = sprintf('%s%s/',rep_display,choice_ling);
			IOControl.create_repertory(rep_display);
		end
		
		function [dim_1,dim_2,tab_val] = initialise_dimensions_O(rep_param)
			[minimum,step,maximum,nb_values] = Dimension.give_dimensions_S_vowel_discretized(rep_param);
			dim_1 = [minimum(1):step(1):maximum(1)];
			dim_2 = [minimum(2):step(2):maximum(2)];			
			tab_val = Sample_OL.perform_redim_2D_1D(dim_1,dim_2);
		end
		
		function [dim_1,dim_2,tab_val] = initialise_dimensions_F(rep_param)
			[minimum,step,maximum,nb_values] = Dimension.give_dimensions_S_cons_discretized(rep_param);
			dim_1 = [minimum(1):step(1):maximum(1)];
			dim_2 = [minimum(2):step(2):maximum(2)];			
			tab_val = Sample_OL.perform_redim_2D_1D(dim_1,dim_2);
		end
		
		function [dim_1_cons,dim_2_cons,dim_1_vowel,dim_2_vowel,tab_val] = initialise_dimensions_S(rep_param)
			[minimum,step,maximum,nb_values] = Dimension.give_dimensions_S_cons_discretized(rep_param);
			dim_1_cons = [minimum(1):step(1)*4:maximum(1)];
			dim_2_cons = [minimum(2):step(2)*4:maximum(2)];
			
			[minimum,step,maximum,nb_values] = Dimension.give_dimensions_S_vowel_discretized(rep_param);
			dim_1_vowel = [minimum(1):step(1)*4:maximum(1)];
			dim_2_vowel = [minimum(2):step(2)*4:maximum(2)];
			
			[A,B,C,D] = ndgrid(dim_2_vowel,dim_1_vowel,dim_2_cons,dim_1_cons);
			A = reshape(A,prod(size(A)),1);
			B = reshape(B,prod(size(B)),1);
			C = reshape(C,prod(size(C)),1);
			D = reshape(D,prod(size(D)),1);
			tab_val = [D,C,B,A];
		end
		
		function mat_f = compute_sample_OL(param,rep_data,tab_val,nb_save,save_matrix,file_name)
			mat_f = cell(nb_save);
			weight_all = load(sprintf('%shist_P_O_weight_%d.txt',rep_data,param.nb_init_o));
			for i=1:param.nb_init_o
				param_gaussian = load(sprintf('%sparameters_P_S_knowing_OL_%d.txt',rep_data,i));
				mu = param_gaussian(:,1:param.nb_dim_s);
				sigma = param_gaussian(:,param.nb_dim_s+1:end);
				for j=1:nb_save
					g = Gaussienne_XD(param.nb_dim_s,mu(j,:),reshape(sigma(j,:),sqrt(size(sigma,2)),sqrt(size(sigma,2))));
					mat_gaussian = g.get_values(tab_val).* weight_all(j,i);
					%mat_gaussian = mat_gaussian ./ sum(mat_gaussian);
					if i == 1
						mat_f{j} = mat_gaussian;
					else
						mat_f{j} = mat_f{j} + mat_gaussian;
					end	
				end
			end
			
			%Normalisation
			for j=1:nb_save
				mat_f{j} = mat_f{j} ./ sum(mat_f{j});
			end
			
			if save_matrix == 1
				save(sprintf('%s%s',rep_data,file_name),'mat_f');
			end
		end
		
		
		function mat_gaussian = compute_sample_OL_by_obj(param,rep_data,tab_val,num_save,save_matrix,file_name)
			weight_all = load(sprintf('%shist_P_O_weight_%d.txt',rep_data,param.nb_init_o));
			for i=1:param.nb_init_o
				param_gaussian = load(sprintf('%sparameters_P_S_knowing_OL_%d.txt',rep_data,i));
				mu = param_gaussian(:,1:param.nb_dim_s);
				sigma = param_gaussian(:,param.nb_dim_s+1:end);
				j = num_save;
				g = Gaussienne_XD(param.nb_dim_s,mu(j,:),reshape(sigma(j,:),sqrt(size(sigma,2)),sqrt(size(sigma,2))));
				mat_gaussian(:,i) = g.get_values(tab_val).* weight_all(j,i);
				mat_gaussian(:,i) = mat_gaussian(:,i) ./ sum(mat_gaussian(:,i));
			end
			
			if save_matrix == 1
				save(sprintf('%s%s',rep_data,file_name),'mat_gaussian');
			end	
		end
		
	end
end
	