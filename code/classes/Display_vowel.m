classdef Display_vowel < Display
	methods(Static)
		
		%------------------------- %
		%--- FONCTIONS SCATTER --- %
		%------------------------- %
		
		function display_scatter_2D_vow(mat,file_name,parameters)
			[figure1, axes1] = Display.begin_figure();		
			
			scatter(mat(:,2),mat(:,1));
			
			axis([250 750 600 2000]);
			
			Display.apply_legend_2D(parameters);			
			Display.apply_reverse_2D();	
			Display.end_figure(figure1, file_name);
		end
	
		function display_scatter_2D(mat,file_name,parameters)
			[figure1, axes1] = Display.begin_figure();		
			
			scatter(mat(:,2),mat(:,1));
			
			Display.apply_legend_2D(parameters);			
			Display.apply_reverse_2D();	
			Display.end_figure(figure1, file_name);
		end
		
		function display_scatter_2D_full(mat_full,nb_obj,file_name,parameters)
			[figure1, axes1] = Display.begin_figure();		
			
			for i=1:nb_obj
				mat = mat_full{i};
				scatter(mat(:,2),mat(:,1));
				hold on;
			end
			
			Display.apply_legend_2D(parameters);			
			Display.apply_reverse_2D();	
			Display.end_figure(figure1, file_name);
		end
		
		function display_scatter_3D_simple(dim,file_name,parameters)
			[figure1, axes1] = Display.begin_figure();			
			
			scatter3(dim(:,1),dim(:,2),dim(:,3),'filled','MarkerFaceColor','flat','MarkerEdgeColor','none','SizeData',25);
			
			view(axes1,[-22 76]);
			Display.apply_legend_3D(parameters);					
			Display.end_figure(figure1, file_name);
		end
		
		function display_scatter_3D_full(dim_full,file_name,parameters)
			[figure1, axes1] = Display.begin_figure();			
			
			for i=1:size(dim_full,2)
				dim = dim_full{i};
				scatter3(dim(:,1),dim(:,2),dim(:,3),'filled','MarkerFaceColor','flat','MarkerEdgeColor','none','SizeData',25);
				hold on;
			end
						
			view(axes1,[-22 76]);
			Display.apply_legend_3D(parameters);					
			Display.end_figure(figure1, file_name);
		end
		
		function display_scatter_3D_full_view_2(dim_full,file_name,parameters)
			[figure1, axes1] = Display.begin_figure();			
			
			for i=1:size(dim_full,2)
				dim = dim_full{i};
				scatter3(dim(:,1),dim(:,2),dim(:,3),'filled',...
                         'MarkerFaceColor','flat',...
                         'MarkerEdgeColor','none',...
                         'SizeData',25);
				hold on;
			end
						
			view(axes1,[-90 0]);
			Display.apply_legend_3D(parameters);					
			Display.end_figure(figure1, file_name);
		end
		
		function display_scatter_3D_full_view_3(dim_full,file_name,parameters)
			[figure1, axes1] = Display.begin_figure();			
			
			for i=1:size(dim_full,2)
				dim = dim_full{i};
				scatter3(dim(:,1),dim(:,2),dim(:,3),'filled','MarkerFaceColor','flat','MarkerEdgeColor','none','SizeData',25);
				hold on;
			end
						
			view(axes1,[0 -90]);
			Display.apply_legend_3D(parameters);					
			Display.end_figure(figure1, file_name);
		end
		
		function display_scatter_3D_full_view_4(dim_full,file_name,parameters)
			[figure1, axes1] = Display.begin_figure();			
			
			for i=1:size(dim_full,2)
				dim = dim_full{i};
				scatter3(dim(:,1),dim(:,2),dim(:,3),'filled','MarkerFaceColor','flat','MarkerEdgeColor','none','SizeData',25);
				hold on;
			end
						
			view(axes1,[180 0]);
			Display.apply_legend_3D(parameters);					
			Display.end_figure(figure1, file_name);
		end
		
		%------------------------- %
		%--- FONCTIONS SURF --- %
		%------------------------- %
		
		function display_surf_2D(mat,dim_1,dim_2,file_name,parameters)			
			[figure1, axes1] = Display.begin_figure();		
			
			surf(dim_1,dim_2,mat);
			Display.apply_legend_2D(parameters);		
			Display.apply_reverse_2D();	
			Display.end_figure(figure1, file_name);
        end
        
		function display_surface_2D(mat,dim_1,dim_2,file_name,parameters)%changed dim order
			[figure1, axes1] = Display.begin_figure();
            surf(dim_1,dim_2,mat,'LineStyle','none');
            view(axes1,[0 90]);
            Display.apply_legend_2D(parameters);		
            Display.apply_reverse_2D();  
			Display.end_figure(figure1, file_name);
        end
        
        function display_contour_2D(mat,data,dim_1,dim_2,file_name,parameters,mu)%changed dim order
            [figure1, axes1] = Display.begin_figure();
            scatter(data(:,1),data(:,2),'.');
            hold on
            contour(dim_1,dim_2,mat);
			Display.add_marker_point(mu(:,1),mu(:,2));
            Display.apply_legend_2D(parameters);		
            Display.apply_reverse_2D();
			Display.end_figure(figure1, file_name);
        end
	
        function display_gaussian_centers(rep_data,file_name,parameters,nb_gaussian, iter)
            param_gaussian=[;;];
            for i = 1:nb_gaussian
                param_gaussian=cat(3,param_gaussian,load(sprintf('%sparameters_P_S_knowing_OL_%d.txt',rep_data,i)));
            end
            [figure1, axes1] = Display.begin_figure();		
            Display.add_marker_point(param_gaussian,iter,nb_gaussian);
            Display.apply_reverse_2D();
            Display.apply_legend_2D(parameters);		
            Display.end_figure(figure1, file_name);
        end
    end
end