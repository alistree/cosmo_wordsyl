classdef Display_PS_knowing_OL
	methods(Static, Access = protected)
				
		function compute_PS_knowing_OL_S(mat_gaussian,file_name_vow,file_name_cons,...
										 dim_1_cons,dim_2_cons,dim_1_vowel,dim_2_vowel)
			
			% Vowels display
			PB_S_knowing_OS_mat = reshape(mat_gaussian,length(dim_1_cons),length(dim_2_cons),length(dim_1_vowel),length(dim_2_vowel));
			PB_S_knowing_OS_mat_vowel_tmp = sum(PB_S_knowing_OS_mat,3);
			PB_S_knowing_OS_mat_vowel_tmp = sum(PB_S_knowing_OS_mat_vowel_tmp,4);
			PB_S_knowing_OS_mat_vowel(:,:) = PB_S_knowing_OS_mat_vowel_tmp;
			parameters_S.legend.x = 'F2'; parameters_S.legend.y = 'F1';
			
			Display_vowel.display_surf_2D(PB_S_knowing_OS_mat_vowel',dim_2_vowel,dim_1_vowel,file_name_vow,parameters_S);
			Display_vowel.display_surface_2D(PB_S_knowing_OS_mat_vowel',dim_2_vowel,dim_1_vowel,file_name_vow,parameters_S);
            
			% Consonant display
		%	PB_S_knowing_OS_mat_cons_tmp = sum(PB_S_knowing_OS_mat,1);
	%		PB_S_knowing_OS_mat_cons_tmp = sum(PB_S_knowing_OS_mat_cons_tmp,2);
	%		PB_S_knowing_OS_mat_cons(:,:) = PB_S_knowing_OS_mat_cons_tmp;
						
	%		Display_cons.display_surface_2D(PB_S_knowing_OS_mat_cons',dim_1_cons,dim_2_cons,file_name_cons);
		end
	end
	
	methods(Static)
	
		function display_PS_knowing_OL_O(rep_param,rep_data,rep_display,nb_save)
			m_syl = Manage_Syllabe();
			
			% Initialisation
			[param,rep_data,rep_display] = Sample_OL.initialize_param(m_syl,m_syl.TYPE_VOW,rep_data,rep_display);
			[dim_1,dim_2,tab_val] = Sample_OL.initialise_dimensions_O(rep_param);
			
			parameters_S.legend.x = 'F2'; parameters_S.legend.y = 'F1';
			
			% Creation ou recovery of gaussian mixture samples
			file_name = 'sample_vow_OL.mat';
			if exist(sprintf('%s%s',rep_data,file_name)) == 2
				load(sprintf('%s%s',rep_data,file_name));
			else
				mat_f = Sample_OL.compute_sample_OL(param,rep_data,tab_val,nb_save,0,file_name);
			end
			
			weight_all = load(sprintf('%shist_P_O_weight_%d.txt',rep_data,param.nb_init_o));
			
			% Display of gaussian mixture samples 
			for j=1:nb_save
				file_name= sprintf('%sdisplay_PS_knowing_OL_%d',rep_display,j);
				PB_S_knowing_OS_mat = reshape(mat_f{j},length(dim_1),length(dim_2));
				Display_vowel.display_surf_2D(PB_S_knowing_OS_mat,dim_2,dim_1,file_name,parameters_S);
				Display_vowel.display_surface_2D(PB_S_knowing_OS_mat,dim_2,dim_1,file_name,parameters_S);
                file_name= sprintf('%sdisplay_PS_knowing_OL_centers_%d',rep_display,j);
                Display_vowel.display_gaussian_centers(rep_data,file_name,parameters_S,param.nb_init_o,j)
				file_name_w= sprintf('%sdisplay_bar_weight_%d',rep_display,j);
				Display.display_bar(weight_all(j,:),file_name_w);
			end
			
			
		end
		
% 		function display_PS_knowing_OL_F(rep_param,rep_data,rep_display,nb_save)
% 			m_syl = Manage_Syllabe();
% 			
% 			Initialisation
% 			[param,rep_data,rep_display] = Sample_OL.initialize_param(m_syl,m_syl.TYPE_CONS,rep_data,rep_display);			
% 			[dim_1,dim_2,tab_val] = Sample_OL.initialise_dimensions_F(rep_param);
% 			
% 			parameters_S.legend.x = 'F2'; parameters_S.legend.y = 'F3';
% 
% 			Creation ou recovery of gaussian mixture samples
% 			file_name = 'sample_cons_OL.mat';
% 			if exist(sprintf('%s%s',rep_data,file_name)) == 2
% 				load(sprintf('%s%s',rep_data,file_name));
% 			else
% 				mat_f = Sample_OL.compute_sample_OL(param,rep_data,tab_val,nb_save,0,file_name);
% 			end		
% 
% 			weight_all = load(sprintf('%shist_P_O_weight_%d.txt',rep_data,param.nb_init_o));
% 
% 			Display of gaussian mixture samples  
% 			for j=1:nb_save
% 				file_name= sprintf('%sdisplay_PS_knowing_OL_%d',rep_display,j);
% 				PB_S_knowing_OS_mat = reshape(mat_f{j},length(dim_1),length(dim_2));
% 				Display_cons.display_surface_2D(PB_S_knowing_OS_mat,dim_1,dim_2,file_name);
% 				file_name_w= sprintf('%sdisplay_bar_weight_%d',rep_display,j);
% 				Display.display_bar(weight_all(j,:),file_name_w);
% 			end		
% 		end
% 		
% 		function display_PS_knowing_OL_S(rep_param,rep_data,rep_display,nb_save)
% 			m_syl = Manage_Syllabe();
% 			
% 			Initialisation
% 			[param,rep_data,rep_display] = Sample_OL.initialize_param(m_syl,m_syl.TYPE_SYL,rep_data,rep_display);
% 			[dim_1_cons,dim_2_cons,dim_1_vowel,dim_2_vowel,tab_val] = Sample_OL.initialise_dimensions_S(rep_param);
% 			
% 			Creation ou recovery of gaussian mixture samples		
% 			file_name = 'sample_syl_OL.mat';
% 			if exist(sprintf('%s%s',rep_data,file_name)) == 2
% 				load(sprintf('%s%s',rep_data,file_name));
% 			else
% 				mat_f = Sample_OL.compute_sample_OL(param,rep_data,tab_val,nb_save,0,file_name);
% 			end
% 			
% 			weight_all = load(sprintf('%shist_P_O_weight_%d.txt',rep_data,param.nb_init_o));
% 
% 			Display of gaussian mixture samples  
% 			for j=1:nb_save		
% 				file_name_vow = sprintf('%sdisplay_PS_knowing_OL_vow_%d',rep_display,j);
% 				file_name_cons = sprintf('%sdisplay_PS_knowing_OL_cons_%d',rep_display,j);
% 				
% 				Display_PS_knowing_OL.compute_PS_knowing_OL_S(mat_f{j},file_name_vow,file_name_cons,...
% 										                      dim_1_cons,dim_2_cons,dim_1_vowel,dim_2_vowel);
% 				file_name_w= sprintf('%sdisplay_bar_weight_%d',rep_display,j);
% 				Display.display_bar(weight_all(j,:),file_name_w);
% 			end
% 			
% 			To have separate gaussian in different figures to detect vowels corresponding to each consonant	% correspondant à chaque consonne
% 			for i=1:param.nb_init_o
% 				param_gaussian = load(sprintf('%sparameters_P_S_knowing_OL_%d.txt',rep_data,i));
% 				mu = param_gaussian(:,1:param.nb_dim_s);
% 				sigma = param_gaussian(:,param.nb_dim_s+1:end);
% 				for j=nb_save:nb_save
% 					g = Gaussienne_XD(param.nb_dim_s,mu(j,:),reshape(sigma(j,:),sqrt(size(sigma,2)),sqrt(size(sigma,2))));
% 					mat_gaussian = g.get_values(tab_val);
% 					
% 					file_name_vow = sprintf('%sdisplay_PS_knowing_OL_vow_sep_%d',rep_display,i);
% 					file_name_cons = sprintf('%sdisplay_PS_knowing_OL_cons_sep_%d',rep_display,i);
% 					
% 					Display_PS_knowing_OL.compute_PS_knowing_OL_S(mat_gaussian,file_name_vow,file_name_cons,...
% 										 dim_1_cons,dim_2_cons,dim_1_vowel,dim_2_vowel);								
% 				end
% 			end
% 		
% 		end
	
	end

end