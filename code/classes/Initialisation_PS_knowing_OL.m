classdef Initialisation_PS_knowing_OL
	
	methods (Static, Access = protected)
		
		function init_rep_parameters(type_obj,num);
			global PMT;
			
			PMT.set_rep(sprintf('%s%s/',PMT.REP,type_obj));
			
			disp(sprintf('Creation des repertoires %s',type_obj));
			IOControl.create_repertory(PMT.REP);
			IOControl.create_repertory(sprintf('%sparameters/',PMT.REP));
			IOControl.create_repertory(sprintf('%sdistrib/',PMT.REP));

			save(sprintf('%sparameters/Parameters.m',PMT.REP),'PMT');
			
			% Sauvegarde des paramètres importants pour le Display après %
			s = sprintf('NB_INIT_O =%d;  \t \n',PMT.NB_INIT_O);
			s = strcat(s, sprintf('NB_DIM_S =%d; \t \n',PMT.NB_DIM_S));
			IOControl.save_text(sprintf('%sparameters/param_display_P_S_knowing_OL_%d.txt',PMT.REP,num),s);
		end
	end
	
	methods	(Static)
	
		function initialise_PS_knowing_OL_O(rep_param)
			global PMT;	
			
			m_syl = Manage_Syllabe();
			PMT.set_choice_ling(m_syl.TYPE_VOW);
			
			[minimum,step,maximum,nb_values] = Dimension.give_dimensions_S_vowel_discretized(rep_param);
			PMT.initialise_dimensions(length(nb_values),minimum,maximum);
						
			Initialisation_PS_knowing_OL.init_rep_parameters(m_syl.TYPE_VOW,m_syl.get_ind_type_obj(m_syl.TYPE_VOW));
		end
		
		function initialise_PS_knowing_OL_F(rep_param)
			global PMT;
			
			m_syl = Manage_Syllabe();
			PMT.set_choice_ling(m_syl.TYPE_CONS);
			
			[minimum,step,maximum,nb_values] = Dimension.give_dimensions_S_cons_discretized(rep_param);
			PMT.initialise_dimensions(length(nb_values),minimum,maximum);
			
			Initialisation_PS_knowing_OL.init_rep_parameters(m_syl.TYPE_CONS,m_syl.get_ind_type_obj(m_syl.TYPE_CONS));
		end
		
		function initialise_PS_knowing_OL_S(rep_param)
			global PMT;	
			
			m_syl = Manage_Syllabe();
			PMT.set_choice_ling(m_syl.TYPE_SYL);
			
			[min_vow,step_vow,max_vow,nb_values_vow] = Dimension.give_dimensions_S_vowel_discretized(rep_param);
			[min_cons,step_cons,max_cons,nb_values_cons] = Dimension.give_dimensions_S_cons_discretized(rep_param);
			
			min_syl = [min_cons,min_vow];
			max_syl = [max_cons,max_vow];
			nb_val_syl = length(nb_values_cons) + length(nb_values_vow);
			
			PMT.initialise_dimensions(nb_val_syl,min_syl,max_syl);
			
			Initialisation_PS_knowing_OL.init_rep_parameters(m_syl.TYPE_SYL,m_syl.get_ind_type_obj(m_syl.TYPE_SYL));
		end
	end
end