classdef Dimension

	methods	(Static)
			
		function [minimum,step,maximum,nb_values] = give_dimensions_M_vowel_discretized(repertory)
			file_name = sprintf('%sparameters/data/Parameters_space_dimension_vowels.txt',repertory); 
			
			VALUES_M_txt{1} = Parsing.parsing_txt(file_name,'VALUES_M{1}'); %TB
			VALUES_M_txt{2} = Parsing.parsing_txt(file_name,'VALUES_M{2}'); %TD
			VALUES_M_txt{3} = Parsing.parsing_txt(file_name,'VALUES_M{3}'); %LH

			M_1 = textscan(VALUES_M_txt{1},'[%f:%f:%f]');
			M_2 = textscan(VALUES_M_txt{2},'[%f:%f:%f]');
			M_3 = textscan(VALUES_M_txt{3},'[%f:%f:%f]');
			
			VALUES_M{1} = Parsing.parsing_value(file_name,'VALUES_M{1}');	
			VALUES_M{2} = Parsing.parsing_value(file_name,'VALUES_M{2}');	
			VALUES_M{3} = Parsing.parsing_value(file_name,'VALUES_M{3}');	
			
			minimum = [M_1{1},M_2{1},M_3{1}];
			step = [M_1{2},M_2{2},M_3{2}];
			maximum = [M_1{3},M_2{3},M_3{3}];
			nb_values = [length(VALUES_M{1}),length(VALUES_M{2}),length(VALUES_M{3})];
		end
	
		function [minimum,step,maximum,nb_values] = give_dimensions_M_cons_discretized(repertory)
			file_name = sprintf('%sparameters/data/Parameters_space_dimension_consonants.txt',repertory);
			
			VALUES_M_txt{1} = Parsing.parsing_txt(file_name,'VALUES_M{1}'); %TB
			VALUES_M_txt{2} = Parsing.parsing_txt(file_name,'VALUES_M{2}'); %TD
			VALUES_M_txt{3} = Parsing.parsing_txt(file_name,'VALUES_M{3}'); %LH
			VALUES_M_txt{4} = Parsing.parsing_txt(file_name,'VALUES_M{4}'); %Apex
			VALUES_M_txt{5} = Parsing.parsing_txt(file_name,'VALUES_M{5}'); %Jaw

			M_1 = textscan(VALUES_M_txt{1},'[%f:%f:%f]');
			M_2 = textscan(VALUES_M_txt{2},'[%f:%f:%f]');
			M_3 = textscan(VALUES_M_txt{3},'[%f:%f:%f]');
			M_4 = textscan(VALUES_M_txt{4},'[%f:%f:%f]');
			M_5 = textscan(VALUES_M_txt{5},'[%f:%f:%f]');
			
			VALUES_M{1} = Parsing.parsing_value(file_name,'VALUES_M{1}');	
			VALUES_M{2} = Parsing.parsing_value(file_name,'VALUES_M{2}');	
			VALUES_M{3} = Parsing.parsing_value(file_name,'VALUES_M{3}');
			VALUES_M{4} = Parsing.parsing_value(file_name,'VALUES_M{4}');
			VALUES_M{5} = Parsing.parsing_value(file_name,'VALUES_M{5}');	
			
			minimum = [M_1{1},M_2{1},M_3{1},M_4{1},M_5{1}];
			step = [M_1{2},M_2{2},M_3{2},M_4{2},M_5{2}];
			maximum = [M_1{3},M_2{3},M_3{3},M_4{3},M_5{3}];
			nb_values = [length(VALUES_M{1}),length(VALUES_M{2}),length(VALUES_M{3}),length(VALUES_M{4}),length(VALUES_M{5})];
		end
		
		% Actuellement en Barks
		function [minimum,step,maximum,nb_values] = give_dimensions_S_vowel_discretized(repertory)
			file_name = sprintf('%sparameters/data/Parameters_space_dimension_vowels.txt',repertory);
			
			VALUES_S_txt{1} = Parsing.parsing_txt(file_name,'VALUES_S{1}'); %F1
			VALUES_S_txt{2} = Parsing.parsing_txt(file_name,'VALUES_S{2}'); %F2

			S_1 = textscan(VALUES_S_txt{1},'[%f:%f:%f]');
			S_2 = textscan(VALUES_S_txt{2},'[%f:%f:%f]');
			
			VALUES_S{1} = Parsing.parsing_value(file_name,'VALUES_S{1}');	
			VALUES_S{2} = Parsing.parsing_value(file_name,'VALUES_S{2}');	
			
			minimum = [S_1{1},S_2{1}];
			step = [S_1{2},S_2{2}];
			maximum = [S_1{3},S_2{3}];
			nb_values = [length(VALUES_S{1}),length(VALUES_S{2})];
		end
		
		% Actuellement en Barks
		function [minimum,step,maximum,nb_values] = give_dimensions_S_cons_discretized(repertory)
			file_name = sprintf('%sparameters/data/Parameters_space_dimension_consonants.txt',repertory);
			
			VALUES_S_txt{1} = Parsing.parsing_txt(file_name,'VALUES_S{2}'); %F2
			VALUES_S_txt{2} = Parsing.parsing_txt(file_name,'VALUES_S{3}'); %F3

			S_1 = textscan(VALUES_S_txt{1},'[%f:%f:%f]');
			S_2 = textscan(VALUES_S_txt{2},'[%f:%f:%f]');
			
			VALUES_S{1} = Parsing.parsing_value(file_name,'VALUES_S{2}');	
			VALUES_S{2} = Parsing.parsing_value(file_name,'VALUES_S{3}');	
			
			minimum = [S_1{1},S_2{1}];
			step = [S_1{2},S_2{2}];
			maximum = [S_1{3},S_2{3}];
			nb_values = [length(VALUES_S{1}),length(VALUES_S{2})];
		end		
		
		function dico_size = give_size_M_vowel(repertory)
			file_name = sprintf('%sparameters/data/Parameters_space_dimension_vowels.txt',repertory);
			
			VALUES_M_txt{1} = Parsing.parsing_txt(file_name,'VALUES_M{1}');
			VALUES_M_txt{2} = Parsing.parsing_txt(file_name,'VALUES_M{2}');
			VALUES_M{3} = Parsing.parsing_txt(file_name,'VALUES_M{3}');

			M_1 = textscan(VALUES_M{1},'[%f:%f:%f]');
			M_2 = textscan(VALUES_M{2},'[%f:%f:%f]');
			M_3 = textscan(VALUES_M{3},'[%f:%f:%f]');
			
			dico_size(1) = [M_1{1}:0.01:M_1{3}]; % Pour savoir la vraie valeur
			dico_size(2) = [M_2{1}:0.01:M_2{3}]; % Pour savoir la vraie valeur
			dico_size(3) = [M_3{1}:0.01:M_3{3}]; % Pour savoir la vraie valeur
		end
		
		function dico_size = give_size_M_cons(repertory)
			file_name = sprintf('%sparameters/data/Parameters_space_dimension_consonants.txt',repertory);
			
			VALUES_M_txt{1} = Parsing.parsing_txt(file_name,'VALUES_M{1}');
			VALUES_M_txt{2} = Parsing.parsing_txt(file_name,'VALUES_M{2}');
			VALUES_M_txt{3} = Parsing.parsing_txt(file_name,'VALUES_M{3}');
			VALUES_M_txt{4} = Parsing.parsing_txt(file_name,'VALUES_M{4}');
			VALUES_M_txt{5} = Parsing.parsing_txt(file_name,'VALUES_M{5}');

			M_1 = textscan(VALUES_M_txt{1},'[%f:%f:%f]');
			M_2 = textscan(VALUES_M_txt{2},'[%f:%f:%f]');
			M_3 = textscan(VALUES_M_txt{3},'[%f:%f:%f]');
			M_4 = textscan(VALUES_M_txt{4},'[%f:%f:%f]');
			M_5 = textscan(VALUES_M_txt{5},'[%f:%f:%f]');
			
			dico_size(1) = [M_1{1}:0.01:M_1{3}]; % Pour savoir la vraie valeur
			dico_size(2) = [M_2{1}:0.01:M_2{3}]; % Pour savoir la vraie valeur
			dico_size(3) = [M_3{1}:0.01:M_3{3}]; % Pour savoir la vraie valeur
			dico_size(4) = [M_4{1}:0.01:M_4{3}]; % Pour savoir la vraie valeur
			dico_size(5) = [M_5{1}:0.01:M_5{3}]; % Pour savoir la vraie valeur
		end
	
	end
	
end