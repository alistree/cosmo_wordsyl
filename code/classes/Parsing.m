classdef Parsing
	methods (Static)
		function value = parsing_txt(file_name,variable)
			%Get string corresponding to the data
			value = Parsing.get_parsing_values(file_name,variable,';');	
		end
		
		function value = parsing_value(file_name,variable)
			%Get string corresponding to the data
			str_buffer = Parsing.get_parsing_values(file_name,variable,';');
			%Recovering value
			value = str2num(str_buffer);
		end

		function value = parsing_matrix(file_name,variable)
			%Get string corresponding to the data
			str_buffer = Parsing.get_parsing_values(file_name,variable,';;');
			%Recovering value
			value = str2num(str_buffer);
		end		
	end
	
	methods (Static, Access = private)
		function str_buffer = get_parsing_values(file_name,variable,end_charac)
			% Reading file
			file = fileread(file_name);
			% Find line corresponding to the variable
			num_line = strfind(file, sprintf('%s ',variable));
			% Find beginning of values corresponding to the variable
			num_line1 = strfind( file(num_line(1):end),'=');
			% Find end of the line
			num_line2 = strfind( file(num_line(1):end),end_charac);
			% Get string corresponding to the data
			str_buffer = file(num_line(1)+num_line1:num_line(1)+num_line2-2);
		end	
	end
end