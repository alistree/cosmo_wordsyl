classdef Succession_Rule < Form
	properties (SetAccess = protected, GetAccess = public)
		weights;   % Weight for each word
		nb_elem;
        nb_g;
        step;
        nw;

        init_method='';
        chosen_method='';      % String 'update all' or 'update max'
	end
	
	methods (Access = private)
	end
	
	methods
		%Constructor
		function obj = Succession_Rule()
        end
        
        function initialize(obj, name, wrd_pmt)
        % Cree un objet 'mixture de gaussienne'
        %
        % Ex: object = Mixture_Gaussienne(N,parameters,gmm_parameters)
        % + N    : nom de la mixture (String)
        % + rep  : repertoire de sauvegarde (string)
        % + gmmparameters=struct<nb,dim,mu,cov,init,meth>:
        %    nb   : Nombre de gaussienne
        %    dim  : dimension de l'espace des gaussiennes
        %    mu   : limites de l'espace d'oû les moyennes initiales
        %          des gaussiennes sont choisies [limit_max, limit,min]
        %    cov  : matrice de covariance initiale
        %    init : nombre d'observations hypothétique avant le
        %          debut de l'apprentisage
        %    meth : Methode d'apprentissage (update_all ou update max)
            obj.initialize@Form(name,'Loi de succession',wrd_pmt);
			obj.nb_elem = wrd_pmt.nb_elem;
            obj.nb_g = wrd_pmt.nb_components;
            obj.set_space_dim(1);
            obj.nw = ones(obj.nb_g,obj.nb_g,obj.nb_elem);
            obj.weights = ones(obj.nb_g,obj.nb_g,obj.nb_elem)./(obj.nb_g^2);
            obj.step = wrd_pmt.step;
        end
		
		% --- Function for mixture's gaussians --- %
		
		function p = add_point(obj,w,g1,g2)
            obj.nw(g1,g2,w) = obj.nw(g1,g2,w) + obj.step;
			obj.weights(:,:,w) = obj.nw(:,:,w)./sum(sum(obj.nw(:,:,w)));
            p = obj.weights(:,:,:);
		end
		
		function proba = get_proba(obj,w)
            proba = obj.weights(:,:,w);
        end
        
        function index = get_word_index(obj,s_id)
            if s_id~= 0
                index = obj.data_input(s_id,1);
            else
                index = 1;
            end
        end
		
		function parameters = get_parameters(obj)
			parameters = 0;
		end
        
        function state_data = to_string(obj,iter)
            state_data = reshape(obj.weights,obj.nb_g,obj.nb_g*obj.nb_elem,[]);
        end
		
		% --- Function for mixture --- %
		
		% --- Setter and getter --- %
        
        function h = get_entropy(obj)
        % Get the global entropy of the gausian mixture
            h = -sum(obj.weights.*log(obj.weights));
        end
	end
end
