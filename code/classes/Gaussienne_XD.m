classdef Gaussienne_XD < handle
    properties (SetAccess = private, GetAccess = public)
		% VARIABLES :
		% To store temporary values from learning :
		%   sum (double)
		%   sum_squares (double)
		%   n (int)
		%   learning_is_over (bool)
		% Others variables : 
		%   mu (double) : mean
		%   cov (double) : covariance matrix
		%   norm (double) : normalization constant
		%   t (container(double,double)) : to store values of gaussian
		%   THRESHOLD (float) : threshold for sigma
		n;
		sum;
		sum_squares;
		mu;
		cov;
        space_dim;
    end

    methods (Access = private)
		function init_gauss(obj,n_dim) 
			obj.space_dim = n_dim;
			obj.mu = zeros(1,n_dim);
			obj.cov = zeros(obj.space_dim,obj.space_dim);
			
			obj.sum = zeros(obj.space_dim,1);
			obj.sum_squares = zeros(obj.space_dim,obj.space_dim);
			obj.n = 0;
		end
	
		function res = compute_gauss(obj,val)
			x1 = val - meshgrid(obj.mu,[1:size(val,1)]);       % Meshgrid Pas si necessaire
            res = mvnpdf(val, obj.mu, obj.cov);
            norm_res = sum(sum(res,1));
            if norm_res ~= 0
				res = res/norm_res;
			else
				disp('ok');
				res(:) = 1/numel(val);
			end
			
        end			
    end
	
	methods

		function obj = Gaussienne_XD(varargin)
        % Constructeur de la classe
        % Initialiser une gaussienne , Gaussienne_XD(dim,init_weight,mu,sigma)
        % - dim :Dimension de l'espace
        % - n : nombre de points existants
        % - mu : Moyenne initiale de la gaussienne 
        % - cov : matrice de covariance initiale
            
			obj.init_gauss(varargin{1});
			
			if nargin == 3
				obj.mu = varargin{2};
				obj.cov = varargin{3};	
						
			elseif nargin == 4				
				obj.n = varargin{2};                       % 
				obj.sum = varargin{2}* varargin{3};        %
				obj.mu = varargin{3};                      %
				obj.sum_squares = varargin{2}*(varargin{4}+ (varargin{3}*varargin{3}.'));
				obj.cov = varargin{4};
				%obj.learning_is_over = false;			
				
			elseif nargin < 3 
				error ('Le nombre d''arguments ne convient pas');
			elseif nargin > 4
				error ('Trop d''arguments pour la gaussienne');	
			end		
        end
		
        function ng = get_ng(obj)
        % Get n(g), iterative N (incremented by 1 or P(g|x=x_i)
            ng = obj.n;
        end
        
	function ok = add_point(obj, val, poids)
	    obj.n = obj.n + poids;
			
        obj.sum = obj.sum + (val.*poids);
        obj.sum_squares = obj.sum_squares + (poids.*(val*val.'));
            
        obj.mu = obj.sum ./ obj.n;
        obj.cov = (obj.sum_squares/obj.n) - ...
                  (obj.mu * obj.mu.');

        end
		
		
		%--- Functions about the table ---
		
		% mat doit être une matrice 2D de taille m= permutation des indices en XD et n = nb de dimension
		% res est une matrice 1D.
		function res = get_values(obj,mat)
            value = obj.compute_gauss(mat);
            res = value;
		end
		
		function res = get_proba(obj,val)
        % Get probability (from pdf) P(val|g)
        %    - val must respect gaussian dimensions
            res = mvnpdf(val,obj.mu,obj.cov);   %Matlab function for multivariate normal law
            %x1 = val-obj.mu;
            %res = exp(-0.5*x1'*inv(obj.cov)*x1);
		end
		
        function entropy=get_entropy(obj)
            entropy = 0.5*log(det(exp(1).*2.*pi.*obj.cov));
        end
        
        function res = to_string(obj)

			res = [obj.space_dim, reshape(obj.mu,1,obj.space_dim), ...
                   reshape(obj.cov,1,obj.space_dim*obj.space_dim), obj.n];
        end
        
% $$$         function res = rand_points(obj,nb_points)
% $$$ 			mu_matrix = repmat(obj.mu,nb_points,1);
% $$$ 			%mu_matrix = reshape(mu_matrix,length(obj.mu),nb_points);
% $$$ 			points = randn(nb_points,obj.space_dim)*obj.cov;
% $$$ 			res = mu_matrix + points;
% $$$         end
  
	end

end
