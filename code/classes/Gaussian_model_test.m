classdef Gaussian_model_test
        properties (Access = public)
            
            gaussian_nb;
            gaussian_mixture;
            space_dim;
            mean;             % Vector of size space_dim*gaussian_number
            variance;
            data_weight;
            data_vect;
            data_hist;
            data_hist_index;
            
            results;
            
            Pg_x;            % Probability of g given x P(g|x)
            single_entropy;  % Each gaussian entropy
            global_entropy;  % Whole mixture entropy
            kl_div;          % Kullback leibler divergence
        end
        
        
        methods
            %Constructor
            function obj = Gaussian_model_test()
%                 obj.gaussian_nb=0;
%             	obj.space_dim=0;
%                 obj.mean=0;             % Vector of size space_dim*gaussian_number
%                 obj.variance=0;
%                 obj.gaussians_weight=[];
%                 obj.gaussian_mixture;
%                 obj.data_weight=0;
%                 obj.data_vect=[];
%                 obj.data_hist=[];
%                 obj.data_hist_index=[];           
    
                global PMT;
                obj.gaussian_nb = PMT.nb;
                obj.space_dim = PMT.dim;
                obj.variance = PMT.var;
                obj.data_weight= PMT.data_weight;               
                obj.data_vect = [];
                
                switch PMT.mu
                  case 'same'
                      obj.mean = (rand()*(PMT.max_m - PMT.min_m) + ...
                                   PMT.min_m)*ones(obj.space_dim,1) ;
                      
                  case 'random'
                    for i=1:obj.space_dim
                        for j=1:obj.gaussian_nb
                            obj.mean(i,j) = (rand()*(PMT.max_m - PMT.min_m) + ...
                                   PMT.min_m);
                        end
                    end
                    %fprintf('Inital means : ');
                    %moyennes_initiales_aff = obj.mean
                  otherwise
                    error(['Mean parameters in incorrect (same or ' ...
                           'random), Please check Parameters configuration file.']);
                end
                
                ratio = [0.3 0.7];
                for i= 1:size(PMT.input(:,1),1)
                  obj.data_vect =  [ obj.data_vect; random('Normal', ...
                                                          PMT.input(i,1), ...
                                                          PMT.input(i,2), ...
                                                          [ceil(PMT.nb_tirages*ratio(i)),1] )];
                end
                    
                obj.data_vect = obj.data_vect(randperm(length(obj.data_vect)));
                obj.data_hist_index = -10:40;
                obj.data_hist = hist(obj.data_vect,obj.data_hist_index);
                obj.data_hist = obj.data_hist/sum(obj.data_hist);
                obj.gaussian_mixture = Mixture_Gaussienne(obj.gaussian_nb, ...
                                                          obj.space_dim, ...
                                                          obj.mean, ...
                                                          obj.variance, ...
                                                          obj.data_weight);                
                                     
                % ---- collect data
                %obj.means = zeros(PMT.nb, PMT.nb_tirages);
                %obj.variances = zeros(PMT.nb, PMT.nb_tirages);
                obj.Pg_x = zeros(PMT.nb, PMT.nb_tirages+1); 
                obj.single_entropy = zeros(PMT.nb_tirages,1);
                obj.results = zeros(PMT.nb_tirages+1,PMT.nb,6);   %Parameter structure tmp
            end
            
            
            
            
            function init_directories(obj)
                global PMT;
                IOControl.create_repertory(PMT.rep);
                %ATTENTION
                delete(sprintf('%srandom_data_vect_%s.txt',PMT.rep,PMT.chosen_method));
                delete(sprintf('%sgaussian_mixture_data_%s.txt',PMT.rep,PMT.chosen_method));
                delete(sprintf('%ssimu_parameters_%s.txt',PMT.rep,PMT.chosen_method));
                
                struct2File(PMT,sprintf('%ssimu_parameters_%s.txt',PMT.rep,PMT.chosen_method));
                
                IOControl.create_repertory(PMT.disp_rep);
                
            end
             
            
            
            
            
            
            function save_simulation_input_data(obj)
                global PMT;

                % save input data
                IOControl.save(sprintf('%srandom_data_vect_%s.txt',PMT.rep,PMT.chosen_method), ...
                                      obj.data_vect,'\t');  
            end
            
            function save_simulation_iteration(obj, PMT)
                             
                IOControl.write(sprintf('%sgaussian_mixture_data_%s.txt' ...
                            ,PMT.rep,PMT.chosen_method), obj.results,'-append','delimiter','\t');
                
            end
            
            
            
            
            function display_simulation_results(obj)
                global PMT;

                nombre_parametres = 6;
                clr = ['m','c','g','y','w','r']     ;           %TODO: add read and load methods into IOControl class
                %clr = [[1 0 1];[0 1 1];[1 0 0];[0 0 0];[1 1 0];[1 1 1] ];
                iteration = [1:PMT.nb_tirages+1]';
                step = 0.01;                        % Pas daffichage des fdp
                data = load(sprintf('%sgaussian_mixture_data_%s.txt', ...
                                    PMT.rep,PMT.chosen_method));  % tirages*nb_gauss*param
                tmp = obj.data_hist(obj.data_hist>0);
                input_entropy = (- sum(tmp.*log(tmp))).*(iteration./iteration); 
                data = reshape(data,PMT.nb_tirages+1,PMT.nb,nombre_parametres);
                x = -10:step:40;
                y = zeros(PMT.nb,size(x,2));
                d = zeros(size(PMT.values_storing,2),size(data,2),size(data,3)) ;
                k=1;
                sz = get(0,'Screensize');
                
                for i = 1:PMT.nb_tirages+1
                    if (i-1)==PMT.values_storing(k)
                        d(k,:,:) = data(i,:,:);
                        k=k+1;
                        fprintf('Saving figure : %d \n',i-1);
                    end
                end
                
                for i = 1:size(d,1)
                    var = reshape(d(i,:,2),PMT.dim,PMT.dim,obj.gaussian_nb);
                    mixt = 	pdf(gmdistribution(d(i,:,1).',var,d(i,:,3)),x.').';
                    h=figure('Name',sprintf('Iter %d',PMT.values_storing(i)),...
                                'Position',sz/2,'Visible','off');
                    bar(obj.data_hist_index,obj.data_hist);

                    for j = 1:obj.gaussian_nb
                        hold on
                        y(j,:)=normpdf(x,d(i,j,1), d(i,j,2));
                        plot(x,y(j,:), 'Color', clr(j), 'LineWidth', 1);
                        ylabel('\propto Probabilite');
                        xlabel('Espace des valeurs');
                    end

                    hold on
                    plot(x,mixt, 'Color', [1 .5 .3], 'LineWidth', 2);
                    hold off
                    legend({'Donnees' '1^{er} noyau' '2^{nd} noyau' 'Mixture'},...
                                'Location','Best','FontSize',8);
                    saveas(h,sprintf('%s%d_test-gm_%s-mu_%s_iter%d', ...
                                   PMT.disp_rep, PMT.nb, ...
                                   PMT.mu, ...
                                   PMT.chosen_method, ...
                                   PMT.values_storing(i)), ...
                                   'epsc');
                    % will create FIG1, FIG2,..
                    %close(h);
                end

                f = figure('Name','Useful measures','Position',sz);
                    subplot(221);plot(iteration,data(:,1,3),'m');
                                 hold on
                                 plot(iteration,data(:,2,3),'c');
                                 %hold on
                                 %plot(iteration,data(:,3,3),'g');

                                 legend('P([G=g_1])','P([G=g_2])');
                                 axis([iteration(1) iteration(end) 0 inf]);
                                 xlabel('Iterations');
                    subplot(222);plot(iteration,data(:,1,1),'m','linewidth',2);hold on
                                 plot(iteration,data(:,2,1),'c', ...
                                      'linewidth',2);hold on
                                 % plot(iteration,data(:,3,1),'g', ...
                                 %     'linewidth',2);

                                 title('Moyennes des noyaux \mu(g)');
                                 axis([iteration(1) iteration(end) 0 inf]);
                                 xlabel('Iterations');
                    subplot(223);plot(iteration,data(:,1,2),'m','linewidth',2);hold on
                                 plot(iteration,data(:,2,2),'c', ...
                                      'linewidth',2);%hold on
                                 %plot(iteration,data(:,3,2),'g', ...
                                 %     'linewidth',2);

                                 title('Variances des noyaux \sigma^2(g)');
                                 axis([iteration(1) iteration(end) 0 inf]);
                                 xlabel('Iterations');
                    subplot(224);plot(iteration,data(:,1,5),'m','linewidth',1);hold on
                                 plot(iteration,data(:,2,5),'c', ...
                                      'linewidth',1);hold on
                                 %plot(iteration,data(:,3,5),'g', ...
                                 %     'linewidth',1);hold on
                                 
                                 plot(iteration,input_entropy,'r',...
                                      'LineStyle','--','linewidth',1);hold on
                                 plot(iteration,data(:,2,6), ...
                                      'Color',[1 .5 .3],'linewidth',2);
                                 axis([iteration(1) iteration(end) 0 inf]);
                                 title('Entropie');
                                 xlabel('Iterations');
                                 legend('1^{er} noyau','2^{nd} noyau','Donnees','Mixture');
                                 
                saveas(f,sprintf('%s%d_test-gm_%s-mu_%s_dynamic', ...
                               PMT.disp_rep, obj.gaussian_nb, ...
                               PMT.mu, ...
                               PMT.chosen_method),'epsc');
            end
            
            function launch_simulation(obj , PMT)
                
                %obj.save_simulation_input_data() ;
                IOControl.write(sprintf('%srandom_data_vect_%s.txt',PMT.rep,PMT.chosen_method)...
                                        ,obj.data_vect,'-append', 'delimiter','\t');
                
                k = 2;                                          %storing values index
                for tmp = 1:obj.gaussian_nb
                    obj.single_entropy(tmp) = obj.gaussian_mixture.gaussians(tmp).get_entropy();
                end
                   
                obj.global_entropy = obj.gaussian_mixture.get_entropy_1d();
                    for j=1:obj.gaussian_mixture.nb_gaussians
                        obj.results(1,j,:) = [obj.gaussian_mixture.gaussians(j).mu, ...
                                          obj.gaussian_mixture.gaussians(j).cov,...
                                          obj.gaussian_mixture.weight_gaussians(j),...
                                          obj.Pg_x(j,1), obj.single_entropy(j) , ...
                                          obj.global_entropy];
                    end

                for i = 1:PMT.nb_tirages
                    
                    obj.Pg_x(:,i+1) = obj.gaussian_mixture.get_proba(obj.data_vect(i)).*(obj.gaussian_mixture.weight_gaussians.');
                    obj.Pg_x(:,i+1) = obj.Pg_x(:,i+1)/sum(obj.Pg_x(:,i+1)); %  normalisation
                    [~,selected] = max(obj.Pg_x(:,i+1));
                    
                    switch PMT.chosen_method   
                      case 'update_max'
                        obj.gaussian_mixture.add_point(selected,obj.data_vect(i), 1);
                                                       
                      case 'update_all'
                        for j = 1:obj.gaussian_mixture.nb_gaussians
                            obj.gaussian_mixture.add_point(j,obj.data_vect(i), obj.Pg_x(j,i+1));
                        end
                                                    
                      otherwise
                        error('Please specify incrementing (update_all or update_max)');
                    end
                                      
                    
                                % Update entropy values
                    for tmp = 1:obj.gaussian_nb
                        obj.single_entropy(tmp) = obj.gaussian_mixture.gaussians(tmp).get_entropy();
                    end
                   
                    obj.global_entropy = obj.gaussian_mixture.get_entropy_1d();
                    for j=1:obj.gaussian_mixture.nb_gaussians
                        obj.results(i+1,j,:) = [obj.gaussian_mixture.gaussians(j).mu, ...
                                          obj.gaussian_mixture.gaussians(j).cov, ...
                                          obj.gaussian_mixture.weight_gaussians(j), ...
                                          obj.Pg_x(j,i+1), obj.single_entropy(j) , ...
                                          obj.global_entropy];
                    end
                     if i == PMT.values_storing(k)
                        disp(sprintf('Saving data %d...',k));
                        k=k+1 ;    % increment storing values index
                     end            
                end
                obj.save_simulation_iteration(PMT);
            end
            
     end
end
      % 
