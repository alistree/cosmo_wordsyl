classdef Mixture_Gaussienne < Form
	properties (SetAccess = protected, GetAccess = public)
		nb_gaussians;       % Number of gaussians
		gaussians;          % Gaussians (object Gaussienne_XD)
		weight_gaussians;   % Weight for each gaussian equals to
                            % P(G)
        learning;
        init_method;
        chosen_method;      % String 'update all' or 'update max'
        init_mu;            % Initialization interval limits
                            % (dimension must agree with specified covariance)
		init_cov;           % Initial covariance
		gw;
		n_init;             % Initial weight (More the weight is high more the adding points have little importance)
	end
	
	methods (Access = private)
	end
	
	methods
		%Constructor
		function obj = Mixture_Gaussienne()
        end
        
        function initialize(obj, name, pmt)
        % Cree un objet 'mixture de gaussienne'
        %
        % Ex: object = Mixture_Gaussienne(N,parameters,gmm_parameters)
        % + N    : nom de la mixture (String)
        % + rep  : repertoire de sauvegarde (string)
        % + gmmparameters=struct<nb,dim,mu,cov,init,meth>:
        %    nb   : Nombre de gaussienne
        %    dim  : dimension de l'espace des gaussiennes
        %    mu   : limites de l'espace d'oû les moyennes initiales
        %          des gaussiennes sont choisies [limit_max, limit,min]
        %    cov  : matrice de covariance initiale
        %    init : nombre d'observations hypothétique avant le
        %          debut de l'apprentisage
        %    meth : Methode d'apprentissage (update_all ou update max)
            obj.initialize@Form(name,'Mixture de gaussiennes',pmt);
			obj.nb_gaussians = pmt.nb;
            obj.set_space_dim(pmt.dim);
            if pmt.dim ~= size(pmt.cov,1)
                error('Space dimension and covariance matrix do not agree')
            end
            if pmt.dim ~= size(pmt.mu,1) || ...
                        size(pmt.mu,2) ~= 2
                error('Space dimension and covariance matrix do not agree')
            end
            obj.init_mu = pmt.mu;
            obj.init_cov = pmt.cov;
			obj.n_init = pmt.n_init;
			obj.chosen_method = pmt.method;
            obj.init_method = pmt.init_method;
            obj.learning = pmt.learning;
            
            %    obj.gw = 200*ones(1,obj.nb_gaussians);
            obj.weight_gaussians = ones(1,obj.nb_gaussians)/obj.nb_gaussians;
			obj.gaussians = Gaussienne_XD.empty(obj.nb_gaussians, 0);
            
            switch obj.init_method
              case 'random'
                for i=1:obj.nb_gaussians
                    random_mu = rand(obj.space_dim,1) .* (obj.init_mu(:,1) - obj.init_mu(:,2)) ...
                        + obj.init_mu(:,2);
                    obj.gaussians(i) = Gaussienne_XD(obj.space_dim,obj.n_init,...
                                                     random_mu,obj.init_cov);
                end
              case 'regular'
                n = floor(sqrt(obj.nb_gaussians));
                xx = (0:n-1)/(n-1);
                yy = xx;
                random_mu = zeros(obj.space_dim,obj.nb_gaussians);
                idx = 1;
                for i=1:n
                    for j=1:n
                        random_mu(:,idx) = [xx(i);yy(j)] .* (obj.init_mu(:,1) - obj.init_mu(:,2)) ...
                                        + obj.init_mu(:,2);
                        idx= idx+1;

                    end
                end
                rnd = 0.5*rand(obj.space_dim,obj.nb_gaussians)-0.25*ones(obj.space_dim,obj.nb_gaussians);
                random_mu = random_mu+rnd;
                random_mu = random_mu(:,randperm(obj.nb_gaussians));
                for i=1:obj.nb_gaussians
                    obj.gaussians(i) = Gaussienne_XD(obj.space_dim,obj.n_init,...
                               random_mu(:,i),obj.init_cov);
                end
                

              case 'mlb'
                for i=1:obj.nb_gaussians
                    pts = rand(obj.space_dim,25) .* repmat(obj.init_mu(:,1) - obj.init_mu(:,2),1,25) ...
                          + repmat(obj.init_mu(:,2),1,25);
                    m_pts = mean(pts, obj.space_dim);
                    v_pts = cov(pts');
                    obj.gaussians(i) = Gaussienne_XD(obj.space_dim, ...
                                                     obj.n_init,m_pts,v_pts);
                end
            end
	
        end
		
		% --- Function for mixture's gaussians --- %
		
        function switch_learning(obj)
            if obj.learning == 1
                obj.learning = 3;
            else
                obj.learning = 1;
            end
        end
        
        
		function selected = add_point(obj,varargin)
            val = varargin{1};
            switch obj.learning
                case 1
                    Pg_x = obj.get_proba(val).*(obj.weight_gaussians.');
                    Pg_x = Pg_x/sum(Pg_x);
                    [~,selected] = max(Pg_x);
                case 2
                    pgw = varargin{2};
                    Pg_x = pgw.*obj.get_proba(val).*(obj.weight_gaussians.');
                    Pg_x = Pg_x/sum(Pg_x);
                    [~,selected] = max(Pg_x);
                case 3
                    selected = varargin{2};
            end
            

            switch obj.chosen_method
              case 'update_max'
                obj.gaussians(selected).add_point(val,1);
              case 'update_all'
                for i = 1:obj.nb_gaussians
                    obj.gaussians(i).add_point(val,Pg_x(i));
                end
              otherwise
                error('Please specify incrementing (update_all or update_max)');
            end
            obj.weight_gaussians = obj.get_ng() ./ sum(obj.get_ng());
            %obj.inc_gaussian_weight(selected); 
            w = obj.weight_gaussians;
		end
		
		function proba = get_proba(obj,val)
            proba = zeros(obj.nb_gaussians,1);
			for i = 1:obj.nb_gaussians
 				proba(i) = (obj.gaussians(i).get_proba(val)); 
 			end
		end
		
		function parameters = get_parameters(obj)
            parameters = zeros(obj.nb_gaussians,obj.space_dim*(1+obj.space_dim));
			for i = 1:obj.nb_gaussians
				mu =  obj.gaussians(i).mu;
				mu = reshape(mu,1,obj.space_dim);
				sig = obj.gaussians(i).cov;
				sig = reshape(sig,1,obj.space_dim * obj.space_dim);
				parameters(i,:) = [mu,sig];
			end
		end
        
        function state_data = to_string(obj,iter)
           % dirtyway TODO Modify it!!!
            state_data = zeros(obj.nb_gaussians, ...
                               size([iter obj.weight_gaussians(1),...
                               obj.gaussians(1).to_string()],2));
            for i = 1:obj.nb_gaussians
                state_data(i,:) = [iter, obj.weight_gaussians(i), ...
                                   obj.gaussians(i).to_string()];
            end
        end
		
		% --- Function for mixture --- %
		
		function points = draw_points(obj,nb_points)
            mean = zeros(obj.nb_gaussians,obj.space_dim);
            var = zeros(obj.space_dim,obj.space_dim,obj.nb_gaussians);
			for i=1:obj.nb_gaussians
				mean(i,:) = obj.gaussians(i).mu;
				var(:,:,i) = obj.gaussians(i).cov;
			end
			
			mixt_gaussians = gmdistribution(mean,var,obj.weight_gaussians);
			points = random(mixt_gaussians,nb_points);
		end
		
		function proba = get_values_proba(obj,points)
            mean = zeros(obj.nb_gaussians,obj.space_dim);
            var = zeros(obj.space_dim,obj.space_dim,obj.nb_gaussians);
            for i=1:obj.nb_gaussians
                mean(i,:) = obj.gaussians(i).mu;
                var(:,:,i) = obj.gaussians(i).cov;
            end
			mixt_gaussians = gmdistribution(mean,var,obj.weight_gaussians);
			proba = pdf(mixt_gaussians,points);
		end
		
		% --- Setter and getter --- %
		
		function g = get_gaussians(obj,i)
			g = obj.gaussians(i);
        end
        
        function inc_gaussian_weight(obj,i)
            obj.gw(i) = obj.gw(i)+1;
            obj.weight_gaussians = obj.gw/sum(obj.gw);
        end
        
        function ng = get_ng(obj)
            ng = zeros(1,obj.nb_gaussians);
            for i =1:obj.nb_gaussians
                ng(i) = obj.gaussians(i).get_ng();
            end
        end
        
        function h = get_entropy(obj,sampling)
        % Get the global entropy of the gausian mixture
            if size(sampling) ~= ([1 3])
                error(['Sampling data vector not correct,ex: [start ' ...
                       'step end].']);
            end
            
            if obj.space_dim == 1
                x = (sampling(1):sampling(2):sampling(3)).';
                px = obj.get_values_proba(x);
            elseif obj.space_dim == 2
                [x y] = meshgrid(sampling(1):sampling(2):sampling(3));
                px = obj.get_values_proba([x(:) y(:)]);
            else
                error('Unexpected space_dim value')
            end
            
            px = px/sum(px);
            %    h = -sum(px.*log(px/d));
            h = -sum(px.*log(px));
        end
	end
end
