classdef Display < handle
	methods(Static, Access = protected)
		function [figure1, axes1] = begin_figure()
			figure1 = figure;
			set(figure1, 'Visible', 'off');
			
			axes1 = axes('Parent',figure1);	
		end
		
		function end_figure(figure1, file_name)
			set(gcf,'PaperPositionMode','auto');
			set(gcf, 'Renderer', 'Painters');
			print(figure1,'-dpng',file_name);
			%close(figure1);
        end
		
        function apply_legend_1D(parameters)
            xlhand = get(gca,'xlabel');
			set(xlhand,'string',parameters.legend_x,'fontsize',15);
        end
        
		function apply_legend_2D(parameters)
			xlhand = get(gca,'xlabel');
			set(xlhand,'string',parameters.legend_x,'fontsize',15);
			ylhand = get(gca,'ylabel');
			set(ylhand,'string',parameters.legend_y,'fontsize',15);
		end
		
		function apply_legend_3D(parameters)
			Display.apply_legend_2D(parameters)
			zlhand = get(gca,'zlabel');
			set(zlhand,'string',parameters.legend.z,'fontsize',15);
		end
		
		function apply_limit_2D(axes1,parameters)
			xlim(axes1,parameters.limit.x);
			ylim(axes1,parameters.limit.y);
		end
		
		function apply_limit_3D(axes1,parameters)
			Display.apply_limit_2D(axes1,parameters)
			zlim(axes1,parameters.limit.z);
		end
		
		function apply_tick_2D(axes1,parameters)			
			set(axes1,'XTick',parameters.ticks.x);
			set(axes1,'YTick',parameters.ticks.y);
		end
		
		function apply_tick_3D(axes1,parameters)			
			Display.apply_tick_2D(axes1,parameters)	
			set(axes1,'ZTick',parameters.ticks.z);
		end
		
		function apply_reverse_choice(val_x,val_y,val_z)			
			if val_x == 1 set(gca,'XDir','reverse'); end
			if val_y == 1 set(gca,'YDir','reverse'); end
			if val_z == 1 set(gca,'ZDir','reverse'); end
		end
		
		function apply_reverse_2D()
			view(-90,90);				
			set(gca,'XDir','reverse');
			%set(gca,'YDir','reverse');	
		end
		
		function apply_reverse_3D()			
			Display.apply_reverse_2D()		
			set(gca,'ZDir','reverse');
		end
		
		function apply_global_param(parameters)	
			set(gca,'FontSize',parameters.ticks.size);		
			title(parameters.title);
        end
		
        % Added by ali to show gaussian cores (not so perfect)
        function add_marker_point(x,y)                  % data contains gaussians parameters
            for i=1:length(x)
                hold on
                plot(x(i),y(i),'r*');                 % plot gaussian center
                text(x(i),y(i),sprintf('%d',i));
            end
            %set(mrk,'Xdata', pos(1), 'YData', pos(2));    % mark (x_pos y_pos) point
        end
	end
		
	methods(Static)
		
			function display_bar(mat,file_name)
				[figure1,axes1] = Display.begin_figure();
				bar(mat);
				Display.end_figure(figure1,file_name);
            end
            
           function display_plot_1D(mat,x,file_name,parameters)
                [figure1, axes1] = Display.begin_figure();		
                plot(x,mat,'Color', [1 .5 .3], 'LineWidth', 2);
                Display.apply_legend_1D(parameters);
                Display.end_figure(figure1, file_name);
            end
            function display_plot_1D_plushist(mat,x,file_name,parameters,input,index)
                [figure1, axes1] = Display.begin_figure();		
                bar(index,input,'FaceColor',[.75 .75 .75],...
                    'EdgeColor',[.75 .75 .75]);    %gray histogram
                hold on
                plot(x,mat,'Color', [1 .5 .3], 'LineWidth', 2);
                Display.apply_legend_1D(parameters);
                Display.end_figure(figure1, file_name);
            end
            function display_multiplot(mat1, name1, mat2,name2, mat3, name3,x,file_name)
                [figure1, axes1] = Display.begin_figure();
                subplot(211); plot(mat1); set(gca,'XTick',x); title(name1);
                subplot(223); plot(mat2); set(gca,'XTick',x); title(name2);
                subplot(224); plot(mat3); set(gca,'XTick',x); title(name3);
                Display.end_figure(figure1, file_name);
            end
            
            function display_multiplot2(mat1, name1, mat2,name2,x,file_name)
                [figure1, axes1] = Display.begin_figure();
                size(x)
                size(mat1)
                size(mat2)
                subplot(211); plot(x,mat1); title(name1);
                subplot(212); plot(x,mat2); title(name2);
                Display.end_figure(figure1, file_name);
            end
                
	end

end
