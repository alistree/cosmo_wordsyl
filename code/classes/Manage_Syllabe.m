classdef Manage_Syllabe < handle
	properties (SetAccess = protected)
		list_vowel
		list_cons
		list_type_obj
		list_param_cons
		list_name_param_cons;
		
		nb_vow
		nb_cons
		
		TYPE_VOW
		TYPE_CONS
		TYPE_SYL
	end
	
	methods
		function obj = Manage_Syllabe()
			obj.TYPE_VOW = 'O';
			obj.TYPE_CONS = 'F';
			obj.TYPE_SYL = 'S';
			obj.list_vowel = {'a','i','u','e','3','o','c'};
			obj.list_cons = {'b','d','g'};
			obj.list_param_cons = [3,4,2];
			obj.list_name_param_cons = {'LipsH','Apex','TD'};
			obj.list_type_obj = {obj.TYPE_VOW,obj.TYPE_CONS,obj.TYPE_SYL}; % Pour savoir si on s'occupe des voyelles, des consonnes ou des syllabes (S) 
			obj.nb_vow = length(obj.list_vowel);
			obj.nb_cons = length(obj.list_cons);
			% Ordre actuellement enregistré des param moteurs : 2, 3, 6, 4, 1 (TB,TD,LH,Apex,Jaw)
		end
		
		function type_obj = get_type_obj(obj,ind_type_obj)
			type_obj = obj.list_type_obj{ind_type_obj};
		end
		
		function ind_type_obj = get_ind_type_obj(obj,type_obj)
			ind_type_obj = find(not(cellfun('isempty', strfind(obj.list_type_obj,type_obj))));
		end
		
		function ind_syl = get_indice_syllabe(obj,ind_vow,ind_cons)
			ind_syl = (ind_vow-1)*obj.nb_cons + ind_cons;
		end
		
		function [ind_vow,ind_cons] = get_indice_vow_cons(obj,ind_syl)
			ind_vow = floor((ind_syl-1)/obj.nb_cons)+1;
			ind_cons = mod(ind_syl-1,obj.nb_cons)+1;
		end
		
		function syl = get_syllabe_ind_vc(obj,ind_vow,ind_cons)
			vow = obj.list_vowel{ind_vow};
			cons = obj.list_cons{ind_cons};
			
			syl = sprintf('%s%s',cons,vow);
		end
		
		function syl = get_syllabe_ind_syl(obj,ind_syl)
			[ind_vow,ind_cons] = obj.get_indice_vow_cons(ind_syl);
			syl = obj.get_syllabe_ind_vc(ind_vow,ind_cons);
		end
		
		function vow = get_vow(obj,ind_vow)
			vow = obj.list_vowel{ind_vow};
		end
		
		function cons = get_cons(obj,ind_cons)
			cons = obj.list_cons{ind_cons};
		end
		
		function num_cons = get_param_cons(obj,ind_cons)
			num_cons = obj.list_param_cons(ind_cons);
		end
		
		function param_cons = get_name_param_cons(obj,ind_cons)
			param_cons = obj.list_name_param_cons{ind_cons};
		end
		
		function ind_cons  = get_ind_cons(obj,cons)
			ind_cons = find(not(cellfun('isempty', strfind(obj.list_cons,cons))));
		end
			
		function nb_vow = get_nb_vow(obj)
			nb_vow = obj.nb_vow;
		end
		
		function nb_cons = get_nb_cons(obj)
			nb_cons = obj.nb_cons;
		end
		
		function nb_syl = get_nb_syl(obj)
			nb_syl = obj.nb_vow * obj.nb_cons;
		end
	end
	
end