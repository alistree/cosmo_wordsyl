clear all;
clc;

%--- Chemin ---%
addpath('./functions');
addpath('./classes');
addpath('parameters');

global PMT;

%--- R�pertoires ---%
rep_param = './';
rep_data = './data/';
rep_parameters = './parameters/data/';
	
disp('Chargement des parametres');
param_global = sprintf('%sParameters.txt',rep_parameters);
param_special = sprintf('%sParameters_OL.txt',rep_parameters);
param_master = sprintf('%sParameters_master_files.txt',rep_parameters);
PMT = Parameters_OL(param_global,param_special,param_master);

for i=PMT.SIMU_MIN:PMT.SIMU_MAX

	PMT.set_seed(i);
	rng(PMT.SEED);
	
	%tab_init_o = [3,4,5,6,7,8,9,10,12,15,20,21,22,25,30,50];
    %tab_init_o = [7,8,9,10,12,15,20,21,22,25,30,40,50]; % Liste des
                                                        % nombre de
                                                        % n G � tester
	tab_init_o = [2];   
    
    for j=1:length(tab_init_o)
		
		nb_o = tab_init_o(j);
		PMT.set_nb_init_o(nb_o);

		PMT.set_rep(sprintf('%ssimu_%d/%d/P_S_knowing_OL/',rep_data,i,nb_o));
		IOControl.create_repertory(sprintf('%sparameters/',PMT.REP));
		copyfile(param_global,sprintf('%sparameters/init_parameters_global.txt',PMT.REP));
		copyfile(param_special,sprintf('%sparameters/init_parameters_special.txt',PMT.REP));
		
		%------------------------------- %
		%--- P_S_knowing_OL voyelles --- %
		%------------------------------- %
		Initialisation_PS_knowing_OL.initialise_PS_knowing_OL_O(rep_param);

		P_S_knowing_OL_O = Learning_PS_knowing_OL.generate_incremental_P_S_knowing_O();
		save(sprintf('%sdistrib/P_S_knowing_OL_O.m',PMT.REP),'P_S_knowing_OL_O');

		%------------------------------- %
		%--- P_S_knowing_OL consonnes -- %
		%------------------------------- %
		%PMT.set_rep(sprintf('%ssimu_%d/%d/P_S_knowing_OL/',rep_data,i,nb_o));
		%Initialisation_PS_knowing_OL.initialise_PS_knowing_OL_F(rep_param);

		%P_S_knowing_OL_F = Learning_PS_knowing_OL.generate_incremental_P_S_knowing_O();
		%save(sprintf('%sdistrib/P_S_knowing_OL_F.m',PMT.REP),'P_S_knowing_OL_F');

		%------------------------------- %
		%--- P_S_knowing_OL syllabes --- %
		%------------------------------- %
		%PMT.set_rep(sprintf('%ssimu_%d/%d/P_S_knowing_OL/',rep_data,i,nb_o));
		%Initialisation_PS_knowing_OL.initialise_PS_knowing_OL_S(rep_param);

		%P_S_knowing_OL_S = Learning_PS_knowing_OL.generate_incremental_P_S_knowing_O();
		%save(sprintf('%sdistrib/P_S_knowing_OL_S.m',PMT.REP),'P_S_knowing_OL_S');
	end
end
