clear all
close all
%clc

addpath('./functions/');
Nc = 49; Nw = 35;
num = 5;

simu1_id = 'far_Dbl_Fast_wordsyl_7-5_v1_49';
simu3_id = 'far_Dbl_Fast_wordsyl_7-5_v3_49';
simu1s_id = 'far_Sl_Fast_wordsyl_7-5_v1_49';
simu3s_id = 'far_Sl_Fast_wordsyl_7-5_v3_49';
simu1s_alt = 'seq_Sl_Fast_wordsyl_7-5_v1_49';
simu1s_alt2 = 'seq2_Sl_Fast_wordsyl_7-5_v1_49';
simu1s_alt3 = 'seq3_Sl_Fast_wordsyl_7-5_v1_49';


x  = [0 1 5 10 15 20 40 50 75 100 150 200 250 300 500 1000 1500 2000 2500 3000 3500 4000 5000 7000 8000 9000 10000 15000 20000 25000 30000 35000 40000 45000 50000 60000 70000 100000 125000 150000 175000 200000 250000 300000 400000:200000:2000000];
xg = [0 1 5 10 15 20 40 50 75 100 150 200 250 300 500 1000 1500 2000 2500 3000 3500 4000 5000 7000 8000 9000 10000 15000 20000 25000 30000 35000 40000 45000 50000 60000 70000 100000 125000 150000 175000 200000 250000 300000 400000:200000:4000000];
t = [0 1 5 10 15 20 40 50 75 100 150 200 250 300 500 1000 1500 2000 2500 3000 3500 4000 5000 7000 8000 9000 10000 15000 20000 25000 30000 35000 40000 45000 50000 60000 70000 100000 125000 150000 175000 200000 250000 300000 400000:200000:600000];

sd= [5 10 20 30:48];
% 
% W_1 = zeros(Nc,Nc,Nw,size(x,2),10);
% W_3 = zeros(Nc,Nc,Nw,size(x,2),10);
% W_1s = zeros(Nc,Nc,Nw,size(x,2),10);
% W_3s = zeros(Nc,Nc,Nw,size(x,2),10);
% W_alt = zeros(Nc,Nc,Nw,size(t,2),10);

Wentropy_1 = zeros(num,size(x,2));
Wentropy_3 = zeros(num,size(x,2));
Wentropy_1s = zeros(num,size(x,2));
Wentropy_3s = zeros(num,size(x,2));
Wentropy_alt = zeros(num,size(t,2));
Wentropy_alt2 = zeros(num,size(t,2));
Wentropy_alt3 = zeros(num,size(t,2));
Wentropy_alt4 = zeros(num,size(t,2));

my= zeros(1,1,5);


for i=1:num
    
    h1 = load(['./data/' simu1_id '/output/' num2str(i) '/G1/kl_div.mat']);
    h12 = load(['./data/' simu1_id '/output/' num2str(i) '/G2/kl_div.mat']);
    h3 = load(['./data/' simu3_id '/output/' num2str(i) '/G1/kl_div.mat']);
    h32 =  load(['./data/' simu3_id '/output/' num2str(i) '/G2/kl_div.mat']);
    hs1 = load(['./data/' simu1s_id '/output/' num2str(i) '/G1/kl_div.mat']);
    hs3 = load(['./data/' simu3s_id '/output/' num2str(i) '/G1/kl_div.mat']);
    ha = load(['./data/' simu1s_alt '/output/' num2str(i) '/G1/kl_div.mat']);
    ha2 = load(['./data/' simu1s_alt2 '/output/' num2str(i) '/G1/kl_div.mat']);
    ha3 = load(['./data/' simu1s_alt3 '/output/' num2str(i) '/G1/kl_div.mat']);
    
    
    Gentropy_1(i,:) =   h1.div;
    Gentropy_3(i,:) =   h3.div;
    Gentropy_12(i,:) =   h12.div;
    Gentropy_32(i,:) =   h32.div;
    Gentropy_1s(i,:) =  hs1.div;
    Gentropy_3s(i,:) =  hs3.div;
    Gentropy_alt(i,:) = ha.div;
    Gentropy_alt2(i,:) = ha2.div;
    Gentropy_alt3(i,:) = ha3.div;
    
	h1 = load(['./data/' simu1_id '/output/' num2str(i) '/W/word_entropy.mat']);
    h3 = load(['./data/' simu3_id '/output/' num2str(i) '/W/word_entropy.mat']);
    hs1 = load(['./data/' simu1s_id '/output/' num2str(i) '/W/word_entropy.mat']);
    hs3 = load(['./data/' simu3s_id '/output/' num2str(i) '/W/word_entropy.mat']);
    ha = load(['./data/' simu1s_alt '/output/' num2str(i) '/W/word_entropy.mat']);
    ha2 = load(['./data/' simu1s_alt2 '/output/' num2str(i) '/W/word_entropy.mat']);
    ha3 = load(['./data/' simu1s_alt3 '/output/' num2str(i) '/W/word_entropy.mat']);
    %ha4 = load(['./data/' simu1s_alt4 '/output/' num2str(i) '/W/word_entropy.mat']);
    
    Wentropy_1(i,:) =   h1.h;
    Wentropy_3(i,:) =   h3.h;
    Wentropy_1s(i,:) =  hs1.h;
    Wentropy_3s(i,:) =  hs3.h;
   Wentropy_alt(i,:) = ha.h(1:size(t,2));
   Wentropy_alt2(i,:) = ha2.h(1:size(t,2));
    Wentropy_alt3(i,:) = ha3.h(1:size(t,2));
%     Wentropy_alt4(i,:) = ha4.h(1:size(t,2));
    
    my1 = load(['./data/' simu1_id '/output/' num2str(i) '/W/mean_yellow.mat']);
    my3 = load(['./data/' simu3_id '/output/' num2str(i) '/W/mean_yellow.mat']);
    my1s = load(['./data/' simu1s_id '/output/' num2str(i) '/W/mean_yellow.mat']);
    my3s = load(['./data/' simu3s_id '/output/' num2str(i) '/W/mean_yellow.mat']);
    myalt = load(['./data/' simu1s_alt '/output/' num2str(i) '/W/mean_yellow.mat']);
    myalt2 = load(['./data/' simu1s_alt2 '/output/' num2str(i) '/W/mean_yellow.mat']);
    myalt3 = load(['./data/' simu1s_alt3 '/output/' num2str(i) '/W/mean_yellow.mat']);
%     myalt4 = load(['./data/' simu1s_alt4 '/output/' num2str(i) '/W/mean_yellow.mat']);
    
    my(:,i,1) = my1.mean_yellow;
    my(:,i,2) = my3.mean_yellow;
    my(:,i,3) = my1s.mean_yellow;
    my(:,i,4) = my3s.mean_yellow;
    my(:,i,5) = myalt.mean_yellow;
    my(:,i,6) = myalt2.mean_yellow;
    my(:,i,7) = myalt3.mean_yellow;
%     my(:,i,8) = myalt4.mean_yellow;
    
    lc_1=load(['./data/' simu1_id '/output/' num2str(i) '/W/line_col.mat']);
    lc_3=load(['./data/' simu3_id '/output/' num2str(i) '/W/line_col.mat']);
    lc_1s=load(['./data/' simu1s_id '/output/' num2str(i) '/W/line_col.mat']);
    lc_3s=load(['./data/' simu3s_id '/output/' num2str(i) '/W/line_col.mat']);
    lc_alt=load(['./data/' simu1s_alt '/output/' num2str(i) '/W/line_col.mat']); 
    lc_alt2=load(['./data/' simu1s_alt2 '/output/' num2str(i) '/W/line_col.mat']); 
    lc_alt3=load(['./data/' simu1s_alt3 '/output/' num2str(i) '/W/line_col.mat']); 
%     lc_alt4=load(['./data/' simu1s_alt4 '/output/' num2str(i) '/W/line_col.mat']); 
    
    
    Wlinecol_1(i,:) = lc_1.cl;
    Wlinecol_3(i,:) = lc_3.cl;
    Wlinecol_1s(i,:) = lc_1s.cl;
    Wlinecol_3s(i,:) = lc_3s.cl;
    Wlinecol_alt(i,:) = lc_alt.cl;
    Wlinecol_alt2(i,:) = lc_alt2.cl;
    Wlinecol_alt3(i,:) = lc_alt3.cl;
    %Wlinecol_alt4(i,:) = lc_alt4.cl;

    
    
    ball1 = load(['./data/' simu1_id '/output/' num2str(i) '/W/all_bars.mat']);
    bmaj1 = load(['./data/' simu1_id '/output/' num2str(i) '/W/major_bars.mat']);
    
    ball3 = load(['./data/' simu3_id '/output/' num2str(i) '/W/all_bars.mat']);
    bmaj3 = load(['./data/' simu3_id '/output/' num2str(i) '/W/major_bars.mat']);
    
    balls1 = load(['./data/' simu1s_id '/output/' num2str(i) '/W/all_bars.mat']);
    bmajs1 = load(['./data/' simu1s_id '/output/' num2str(i) '/W/major_bars.mat']);
    
    balls3 = load(['./data/' simu3s_id '/output/' num2str(i) '/W/all_bars.mat']);
    bmajs3 = load(['./data/' simu3s_id '/output/' num2str(i) '/W/major_bars.mat']);
    
    ball_alt = load(['./data/' simu1s_alt '/output/' num2str(i) '/W/all_bars.mat']);
    bmaj_alt = load(['./data/' simu1s_alt '/output/' num2str(i) '/W/major_bars.mat']);
    
        ball_alt2 = load(['./data/' simu1s_alt2 '/output/' num2str(i) '/W/all_bars.mat']);
    bmaj_alt2 = load(['./data/' simu1s_alt2 '/output/' num2str(i) '/W/major_bars.mat']);
        ball_alt3 = load(['./data/' simu1s_alt3 '/output/' num2str(i) '/W/all_bars.mat']);
    bmaj_alt3 = load(['./data/' simu1s_alt3 '/output/' num2str(i) '/W/major_bars.mat']);
%         ball_alt4 = load(['./data/' simu1s_alt4 '/output/' num2str(i) '/W/all_bars.mat']);
%     bmaj_alt4 = load(['./data/' simu1s_alt4 '/output/' num2str(i) '/W/major_bars.mat']);
    
    Abars(i,:,1) = ball1.all_bars;
    Abars(i,:,2) = ball3.all_bars;
    Abars(i,:,3) = balls1.all_bars;
    Abars(i,:,4) = balls3.all_bars;
    Abars(i,:,5) = ball_alt.all_bars;
    Abars(i,:,6) = ball_alt2.all_bars;
    Abars(i,:,7) = ball_alt3.all_bars;
%     Abars(i,:,8) = ball_alt4.all_bars;
    
    Mbars(i,:,1) = bmaj1.major_bars;
    Mbars(i,:,2) = bmaj3.major_bars;
    Mbars(i,:,3) = bmajs1.major_bars;
    Mbars(i,:,4) = bmajs3.major_bars;
    Mbars(i,:,5) = bmaj_alt.major_bars;
    Mbars(i,:,6) = bmaj_alt2.major_bars;
    Mbars(i,:,7) = bmaj_alt3.major_bars;
%     Mbars(i,:,8) = bmaj_alt4.major_bars;
end

Mbars = permute(Mbars,[1 3 2]);
Abars = permute(Abars,[1 3 2]);
Ideal_case = log(35)*ones(1,length(x));

Wentropy_1 = (Wentropy_1/35 + log(35)*ones(num,size(x,2)));
Wentropy_3 = (Wentropy_3/35 + log(35)*ones(num,size(x,2)));
Wentropy_1s = (Wentropy_1s/35 + log(35)*ones(num,size(x,2)));
Wentropy_3s = (Wentropy_3s/35 + log(35)*ones(num,size(x,2)));
Wentropy_alt = (Wentropy_alt/35 + log(35)*ones(num,size(t,2)));
Wentropy_alt2 = (Wentropy_alt2/35 + log(35)*ones(num,size(t,2)));
Wentropy_alt3 = (Wentropy_alt3/35 + log(35)*ones(num,size(t,2)));
Wentropy_alt4 = (Wentropy_alt4/35 + log(35)*ones(num,size(t,2)));

mean_Wentropy_1 = mean(Wentropy_1);
sd_Wentropy_1 = std(Wentropy_1);
mean_Wentropy_3 = mean(Wentropy_3);
sd_Wentropy_3 = std(Wentropy_3);

mean_Wentropys_1 = mean(Wentropy_1s);
sd_Wentropys_1 = std(Wentropy_1s);
mean_Wentropys_3 = mean(Wentropy_3s);
sd_Wentropys_3 = std(Wentropy_3s);

mean_Wentropy_alt = mean(Wentropy_alt);
sd_Wentropy_alt = std(Wentropy_alt);
mean_Wentropy_alt2 = mean(Wentropy_alt2);
sd_Wentropy_alt2 = std(Wentropy_alt2);
mean_Wentropy_alt3 = mean(Wentropy_alt3);
sd_Wentropy_alt3 = std(Wentropy_alt3);
mean_Wentropy_alt4 = mean(Wentropy_alt4);
sd_Wentropy_alt4 = std(Wentropy_alt4);

mean_Gentropy_1 = mean(Gentropy_1);
mean_Gentropy_12 = mean(Gentropy_12);
mean_Gentropy_32 = mean(Gentropy_32);
mean_Gentropy_3 = mean(Gentropy_3);
sd_Gentropy_3 = std(Gentropy_3);
sd_Gentropy_1 = std(Gentropy_1);

mean_Gentropys_1 = mean(Gentropy_1s);
sd_Gentropys_1 = std(Gentropy_1s);
mean_Gentropys_3 = mean(Gentropy_3s);
sd_Gentropys_3 = std(Gentropy_3s);

mean_Gentropy_alt = mean(Gentropy_alt);
sd_Gentropy_alt = std(Gentropy_alt);
mean_Gentropy_alt2 = mean(Gentropy_alt2);
sd_Gentropy_alt2 = std(Gentropy_alt2);
mean_Gentropy_alt3 = mean(Gentropy_alt3);
sd_Gentropy_alt3 = std(Gentropy_alt3);


%h1_fit = fit(x',mean_Wentropy_1','exp2')
%h3_fit = fit(x',mean_Wentropy_3','exp2')
%hs1_fit = fit(x',mean_Wentropys_1','exp2')
%hs3_fit = fit(x',mean_Wentropys_3','exp2')
     
my(isnan(my)) = 0;


 f13=figure();
        barwitherr(sqrt([var(my(:,:,1));var(my(:,:,2));var(my(:,:,3));var(my(:,:,4));var(my(:,:,5));var(my(:,:,6));var(my(:,:,7))]),...
               [mean(my(:,:,1));mean(my(:,:,2));mean(my(:,:,3));mean(my(:,:,4));mean(my(:,:,5));mean(my(:,:,6));mean(my(:,:,7))]);
        %l=legend({'G_1', 'G_2'});set(l, 'Location', 'southeast');
        set(gca,'XTickLabel',{'V1', 'V2','V''1', 'V''2','alt','alt2','alt3'})
        title('Figure13');
        print(f13,'-dpng','figure13');


f1=figure()	;
     z = plot(x,Ideal_case,'r');
     hold on
     a = plot(x, mean_Wentropy_1,'b');
     hold on
     b = plot(t, mean_Wentropy_alt,'m-.');
     hold on
     c = plot(t, mean_Wentropy_alt2,'g-.');
     hold on
     d = plot(t, mean_Wentropy_alt3,'r-.');
     %c = plot(x,mean_Wentropys_1,'m');
     %errorbar(x, mean_Wentropys_1, sd_Wentropys_1,'m');
     %hold on
     %d = plot(x,mean_Wentropys_3,'k');
     %errorbar(x, mean_Wentropys_3, sd_Wentropys_3,'k');
     axis([x(1) t(end) 2 max(mean_Wentropy_3)+1]);
     ll=legend([z,a,b,c,d],{'$ln(\frac{1}{35})$', '\emph{Indep}', '\emph{Seq} $10000$', '\emph{Seq} $50000$', '\emph{Seq} $150000$'});
     set(ll,'interpreter','latex');
     xlabel('It\''erations','interpreter', 'latex');ylabel('Entropie','interpreter', 'latex');
     title('Evolution de l''entropie $H(\sum_{W} P(G_1,~G_2~|~W))$','interpreter','latex');
     print(f1,'-depsc','figure1');
     
     
     f=figure();
     barwitherr(sqrt([var(my(:,:,3));var(my(:,:,4));var(my(:,:,7));var(my(:,:,6));var(my(:,:,5))]),...
             [mean(my(:,:,3));mean(my(:,:,4));mean(my(:,:,7));mean(my(:,:,6));mean(my(:,:,5))]);
     set(gca,'XTickLabel',{'BtUp', 'FdBk','seq = 1500000','seq = 50000','seq = 10000'});
        ylabel('Moyenne','interpreter', 'latex');
        title('Somme des poids des noyaux de poids faibles $\le 0.01$','interpreter','latex');
        print(f,'-depsc','ypointsAlt');

 f=figure();
        barwitherr([std(Wlinecol_1s);std(Wlinecol_3s);std(Wlinecol_alt3);std(Wlinecol_alt2);std(Wlinecol_alt)],...
                    [mean(Wlinecol_1s);mean(Wlinecol_3s);mean(Wlinecol_alt3);mean(Wlinecol_alt2);mean(Wlinecol_alt)]);
        l=legend({'Marginalisation sur $G_1$', 'Marginalisation sur $G_2$'});set(l, 'Location', 'southwest');
        set(l,'interpreter','latex');
        set(gca,'XTickLabel',{'BtUp','FdBk','seq = 1500000','seq = 50000','seq = 10000'});
        ylabel('Nombre','interpreter', 'latex')
        title('Nombre de prototypes sur $G_1$ et sur $G_2$','interpreter','latex');
        print(f,'-depsc','nblinecolAlt');
     
     
     
 f12=figure();
        barwitherr([std(Wlinecol_1);std(Wlinecol_3)],...
                    [mean(Wlinecol_1);mean(Wlinecol_3)]);
        l=legend({'Lignes / $G_1$', 'Colonnes / $G_2$'});set(l, 'Location', 'northwest');
        set(l,'interpreter','latex');
        set(gca,'XTickLabel',{'Indep','Conj'});
        xlabel('M\''ethode','interpreter', 'latex');ylabel('Nombre','interpreter', 'latex')
        title('Nombre de ligne et de colonnes','interpreter','latex');
        print(f12,'-depsc','figure12');
     
 f13=figure();
        barwitherr(sqrt([var(my(:,:,1));var(my(:,:,2))]),...
               [mean(my(:,:,1));mean(my(:,:,2))],0.2);
        %l=legend({'G_1', 'G_2'});set(l, 'Location', 'southeast');
        set(gca,'XTickLabel',{'Indep','Conj'});
        xlabel('M\''ethode','interpreter', 'latex');ylabel('Moyenne','interpreter', 'latex');
        title('Moyenne des poids $\le 0.01$','interpreter','latex');
        print(f13,'-depsc','figure13');
        
        
%         

        
f21=figure()	;
     z = plot(x,Ideal_case,'r');
     hold on
     a=errorbar(x, mean_Wentropy_1, sd_Wentropy_1,'b');
     hold on
     c=errorbar(x, mean_Wentropys_1, sd_Wentropys_1,'m-.');
     axis([x(1) t(end) 2 max(mean_Wentropy_3)+1]);
     ll=legend([z,a,c],{'$ln(\frac{1}{35})$', '$\pi_D$, \emph{Indep}', '$\pi_C$, \emph{Indep}'});
     set(ll, 'interpreter','latex');
     xlabel('It\''erations','interpreter', 'latex');ylabel('Entropie','interpreter', 'latex');
     title('Evolution de l''entropie $H(\sum_{W} P(G_1,~G_2~|~W))$','interpreter','latex');
     print(f21,'-depsc','figure21');
     
 f22=figure();
     z = plot(x,Ideal_case,'r');
     hold on
     b = errorbar(x, mean_Wentropy_3, sd_Wentropy_3,'m');
     hold on
     d = errorbar(x, mean_Wentropys_3, sd_Wentropys_3,'g-.');
     axis([x(1) t(end) 2 max(mean_Wentropy_3)+1]);
     ll=legend([z,b,d],{'$ln(\frac{1}{35})$', '$\pi_D$, \emph{Conj}', '$\pi_C$, \emph{Conj}'});
     set(ll, 'interpreter','latex');
     xlabel('It\''erations','interpreter', 'latex');ylabel('Entropie','interpreter', 'latex');
     title('Evolution de l''entropie $H(\sum_{W} P(G_1,~G_2~|~W))$','interpreter','latex');
     print(f22,'-depsc','figure22');
     
  f23=figure();
    barwitherr([std(Wlinecol_1);std(Wlinecol_1s)],...
           [mean(Wlinecol_1);mean(Wlinecol_1s)]);
    l=legend({'G_1', 'G_2'});set(l, 'Location', 'southeast');
    set(gca,'XTickLabel',{'$\pi_D$, \emph{Conj}', '$\pi_C$, \emph{Conj}'})
    title('Figure23');
    print(f23,'-dpng','figure23');
    
  f24=figure();
    barwitherr(sqrt([var(Wlinecol_3);var(Wlinecol_3s)]),...
           [mean(Wlinecol_3);mean(Wlinecol_3s)]);
    l=legend({'G_1', 'G_2'});set(l, 'Location', 'southeast');
    set(gca,'XTickLabel',{'V2', 'V''2'})
    title('Figure24');
    print(f24,'-dpng','figure24');
        
  f31=figure();
     z = plot(x,Ideal_case,'r');
     hold on
     b = plot(x,mean_Wentropys_1,'m');
     errorbar(x, mean_Wentropys_1, sd_Wentropys_1,'m');
     hold on
     d = plot(x,mean_Wentropys_3,'g');
     errorbar(x, mean_Wentropys_3, sd_Wentropys_3,'g');
     hold on
     c = plot(t,mean_Wentropy_alt,'b');
     errorbar(t, mean_Wentropy_alt, sd_Wentropy_alt,'b');
     axis([t(1) t(end) 2 max(mean_Wentropy_3)+1]);
     legend([z,b,d,c],{'ln(1/35)', 'Version'' 1', 'Version'' 2','Version'' 3'});
     xlabel('Iterations');ylabel('$H(\sum_{W} P(G_1,~G_2))$','interpreter','latex');
     title('Figure31');
     print(f31,'-dpng','figure31');
     
  f32=figure();
    barwitherr([std(Wlinecol_1s);std(Wlinecol_3s);std(Wlinecol_alt);],...
           [mean(Wlinecol_1s);mean(Wlinecol_3s);mean(Wlinecol_alt)]);
    l=legend({'G_1', 'G_2'});set(l, 'Location', 'southeast');
    set(gca,'XTickLabel',{'V''1', 'V''2','V''3'})
    title('Figure32');
    print(f32,'-dpng','figure32');
     
      
 f33=figure();
        barwitherr([std(my(:,:,3));std(my(:,:,4));std(my(:,:,5))],...
               [mean(my(:,:,3));mean(my(:,:,4));mean(my(:,:,5))]);
        %l=legend({'G_1', 'G_2'});set(l, 'Location', 'southeast');
        set(gca,'XTickLabel',{'V''1', 'V''2','V''3'})
        title('Figure13');
        print(f33,'-dpng','figure33');
    
    error('END!')

     
     
     %figure();
     %plot(t,mean_Wentropy_alt,'b');
     %errorbar(t, mean_Wentropy_alt, sd_Wentropy_alt,'b');
     %title('Entropie variante 3');
     
% $$$ f2=figure()
% $$$      title('$H(\sum_{W} P(G_1,~G_2))$','interpreter','latex');
% $$$      subplot(221);plot(h1_fit,x,mean_Wentropy_1);title('Version 1'); ...
% $$$          xlabel('Iterations');ylabel('Mean entropy (V_1)');
% $$$      subplot(222);plot(h3_fit,x,mean_Wentropy_3);title('Version 2'); ...
% $$$          xlabel('Iterations');ylabel('Mean entropy (V_2)');
% $$$ 
% $$$      subplot(223);plot(hs1_fit,x,mean_Wentropys_1);title('Version 1'); ...
% $$$          xlabel('Iterations');ylabel('Mean entropy (V''_1)');
% $$$      subplot(224);plot(hs3_fit,x,mean_Wentropys_3);title('Version 2'); ...
% $$$          xlabel('Iterations');ylabel('Mean entropy (V''_2)');
% $$$      print(f2,'-depsc','Fit_exp_V1V3');
     
 f3=figure();
    subplot(121);boxplot(Abars(:,:,end),origin);title('>.0001')  ;
    subplot(122);boxplot(Mbars(:,:,end),origin);title('>.01')  ;
    print(f3,'-depsc','boxplot');
    
    
    
 f4=figure();
 barwitherr(sqrt([var(Wlinecol_1);var(Wlinecol_3);var(Wlinecol_1s);var(Wlinecol_3s)]),...
               [mean(Wlinecol_1);mean(Wlinecol_3); ...
                mean(Wlinecol_1s);mean(Wlinecol_3s)]);
 l=legend({'G_1', 'G_2'});set(l, 'Location', 'southeast');
 set(gca,'XTickLabel',{'V1', 'V2','V'' 1', 'V'' 2'})
 print(f4,'-depsc','barplot');
    
 f5 = figure();
 subplot(121);plot(x,permute(mean(Abars),[2 3 1]));
 legend({'V1', 'V2','V'' 1', 'V'' 2'})
 xlabel('Iterations');
 %errorbar(x, permute(mean(Abars),[2 3 1]), permute(sqrt(var(Abars)),[2 3 1]));
 title('>.0001')  ;
 subplot(122);plot(x,permute(mean(Mbars),[2 3 1]));
 legend({'V1', 'V2','V'' 1', 'V'' 2'})
 xlabel('Iterations');
 %errorbar(x, permute(mean(Mbars),[2 3 1]), permute(sqrt(var(Mbars)),[2 3 1]));
 title('>.01')  ;
 print(f3,'-depsc','plot_nombrenoyau');