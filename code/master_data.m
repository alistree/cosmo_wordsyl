clc;
clear all
close all
addpath('./functions');
addpath('./classes');
rng(1) % :)
data_vect = [];
for i=1:7
    tmp = load(sprintf('vowel/data_PB_S_knowing_OS_%d.txt',i));
    data_vect = [data_vect; tmp];
end
data_vect = 7*asinh(data_vect/650);

%hyp_shwa = mvnrnd( [4.5 9.5], [0.02 0;0 0.02], 5000);
%data_vect = [data_vect; hyp_shwa];

ordering1 = randperm(length(data_vect));
ordering2 = randperm(length(data_vect));
ordering3 = randperm(length(data_vect));
tmp_mat = data_vect(ordering1,:);
tmp_mat2 = data_vect(ordering2,:);
tmp_mat3 = data_vect(ordering3,:);
data_vect = [tmp_mat; tmp_mat2;tmp_mat3];
        %tmp = hyp_shwa(1:20,:)
        %data_vect = [tmp; data_vect];
        %figure;plot(data_vect(:,1),data_vect(:,2),'.');
data_vect = repmat(data_vect,3,1);
delete('vowel/master_data_rand.txt');
IOControl.save('vowel/master_data_rand.txt', data_vect);