clc;
clear all
close all
addpath('./functions');
addpath('./classes');
rng(1) % :)

vowel_data = zeros(5000,2,2);
GSyl1 = 7;
GSyl2 = 5;
%Load all vowels
for i=1:7
    vowel_data(:,:,i) =load(sprintf('vowel/data_PB_S_knowing_OS_%d.txt',i));
end
% Convert to bark
vowel_data = 7*asinh(vowel_data/650);

%% Prepare random Word List
[X Y]  = meshgrid([1:GSyl1],[1:GSyl2]);
word_list = cat(2,X(:),Y(:));
tmp = zeros(60000*size(word_list,1),size(word_list,2));
prm = zeros(60000*size(word_list,1),1);
for i=1:60000
    rnd = randperm(length(word_list));
    tmp((i-1)*size(word_list,1)+1:i*size(word_list,1),:) = word_list(rnd,:);
    prm((i-1)*size(word_list,1)+1:i*size(word_list,1),:) = rnd';
end
word_list = tmp;

%% Prepare associated vowels
master_data = zeros(length(word_list),2,2); %  2 for dimension
for i=1:length(word_list)
    master_data(i,:,1) = vowel_data(randi(5000),:,word_list(i,1));  
    master_data(i,:,2) = vowel_data(randi(5000),:,word_list(i,2));
end


figure;
subplot(121);plot(master_data(:,1,1),master_data(:,2,1),'r.');
subplot(122);plot(master_data(:,1,2),master_data(:,2,2),'c.');

dt_id = sprintf('%d-%d_',GSyl1,GSyl2)

delete(['vowel/' dt_id 'syl_master_data_rand.txt']);
delete(['vowel/' dt_id 'wrd_master_data_rand.txt']);
IOControl.save(['vowel/' dt_id 'syl_master_data_rand.txt'], master_data);
IOControl.save(['vowel/' dt_id 'wrd_master_data_rand.txt'], cat(2,prm,word_list));