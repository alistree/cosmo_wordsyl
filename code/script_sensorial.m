% Script test form and GMM
clear all;
close all;
clc;

addpath('./functions');
addpath('./classes');

simu_id = '16ker-2-ml';
rep = ['./data/' simu_id '/'];
disp_rep = ['./display/' simu_id '/'];        

%% Initialisation

for sd=1:1
        %% Parameters structure
        % To put else where take too much space ..... :p
        gmm_pmt = struct( ...
            'nb',        16, ...
            'dim',       2, ...   
            'mu',        [6 10;4 8]', ...
            'cov',       [4 0;0 4], ...
            'n_init',    25, ...
            'method',    'update_max', ...,
            'init_method',    'random' ... ,
        );

        forms_pmt = struct( ...
            'name',             'test_2D',...
            'type',             'GM',...
            'tirages',          50000,...
            'sampling',         [0:0.1:10], ...
            'values_storing',   [0 2 5 15 20 40 50 75 100 150 200 250 300 500 1000 1500 2000 2500 3000 3500 4000 5000 7000 8000 9000 10000 15000 20000 25000 30000 35000 40000 45000 50000 60000 70000 100000 125000 150000 175000 200000 250000 300000], ...
            'data_input',       [rep 'input/' num2str(sd) '/'], ...
            'data_output',      [rep 'output/' num2str(sd) '/'], ...
            'disp_output',      [disp_rep num2str(sd) '/'],...
            'space_dim',        gmm_pmt.dim,...
            'elm',              gmm_pmt.nb,...
            'measure',          4 + gmm_pmt.dim^2 + gmm_pmt.dim,...
            'iter',             [0 2 5 15 20 40 50 75 100 150 200 250 300 500 1000 1500 2000 2500 3000 3500 4000 5000 7000 8000 9000 10000 15000 20000 25000 30000 35000 40000 45000 50000 60000 70000 100000 125000 150000 175000 200000 250000 300000], ...
            'legend_x',         'F1',...
            'legend_y',         'F2'...,
        );
        rng(sd);

        %%   Input data (Master data)

        %% Init forms
        disp('Charging parameters...')
        gmm = Mixture_Gaussienne();
        gmm.initialize('test',forms_pmt,gmm_pmt);
        gmm.init_directories();
        gmm.load_input();
        disp('End of init');
        
        %% Learning / Fitting 
        if forms_pmt.tirages > length(gmm.data_input)
        	error('Insufficient data!');
        end
        for i=0:forms_pmt.tirages
            gmm.launch_iteration(i);

        end
        c = gmm.save_simulation_into_file();
        
        s = Display_form();
        s.initialize(forms_pmt);
        s.display_distribution(forms_pmt);
end