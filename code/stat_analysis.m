%clear all
%close all
clc

simu_id = '20ker';
x = [0 2 5 15 20 40 50 75 100 150 200 250 300 500 1000 1500 2000 2500 3000 3500 4000 5000 7000 8000 9000 10000 15000 20000 25000 30000 35000 40000 45000 50000 60000 70000 100000];            

for i=1:5
	h = load(['data\' simu_id '\output\' num2str(i) '\agent_entropy.mat']);
    d = load(['data\' simu_id '\output\' num2str(i) '\kl_div.mat']);
    agent_entropy(i,:) = h.h;
	kl_divergence(i,:) = d.div;
end	
load(['data\' simu_id '\output\' num2str(i) '\master_entropy.mat']);


mean_entropy = mean(agent_entropy);
mean_divergence = mean(kl_divergence);
sd_entropy = sqrt(var(agent_entropy));
sd_divergence = sqrt(var(kl_divergence));

figure()	
     plot(x,mean_entropy,'Color', [1 .5 .3], 'LineWidth', 2);
     errorbar(x,mean_entropy,sd_entropy);
     axis([x(1) x(end) 2 max(mean_entropy)+1]);
     hold on
     plot(x,h_master,'r');
     title('Entropy')

figure()	
     plot(x,mean_divergence,'Color', [1 .5 .3], 'LineWidth', 2);
     errorbar(x,mean_divergence,sd_divergence);
     axis([x(1) x(end) 0 max(mean_divergence)+1]);
     title('KL Divergence')